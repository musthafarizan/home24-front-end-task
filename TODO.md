### Todo
- Use `paths` mapper as I have used in this project for `types`. for other other folders need to configure webpack files also, then import statements are will be more clear and there will be less issues when folders are moved or renamed.
    ```jsx
    import Button from '@components/Button';
    ```

- Add more `eslint` rules and need to configure a pre commit hook to force the eslint practices.

- Integrate `prettier` library to have an proper formatted codes.

- Write unit test for each and every components also for helper functions using react testing library

- Expand `theme` object to support predefined font-sizes, spacing etc...

- Add `i18n` library to support localization.

- Expanding the code base to support `.env` file data, so can proxy url kind of data according to the environment.

- Add a global level error handling for API fail and show a toast message.