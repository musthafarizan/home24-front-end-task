export type ThemeColor = {
    dark: string;
    light: string;
    main: string;

}

export type ThemeColorPalette = {
    primary: ThemeColor;
    grey: ThemeColor;
    black: ThemeColor;
}

export type MediaBreakPointKeys = 'xs' | 'sm' | 'md' | 'lg' | 'xl' | 'xxl';

export type MediaBreakPoints = Record<MediaBreakPointKeys,number>;

export type MediaDevices = Record<MediaBreakPointKeys, string>;

export type Theme = {
    colors: ThemeColorPalette;
    devices: MediaDevices
}