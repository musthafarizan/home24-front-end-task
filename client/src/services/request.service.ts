import axios from "axios";

export default async function request(query: string) {
    return axios.request(
        {
            method: 'POST',
            url: '/graphql',
            headers: {
                'Content-Type': 'application/json'
            },
            data: {
                query,
            },
        }
    )
}