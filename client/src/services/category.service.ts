import { CategoriesListResponse } from "$types/category";
import { ApiResponse } from "$types/response";
import { getCategoryListQuery } from "./quries/categories";
import request from "./request.service";

async function getAllById(): Promise<ApiResponse<CategoriesListResponse> | undefined> {
    try {
        const query = getCategoryListQuery();
        const response = await request(query)
        return response.data;
    } catch (ex) {
        console.log('error', ex);
        return;
    }
}


const categoryService = {
    getAllById
}

export default categoryService;
