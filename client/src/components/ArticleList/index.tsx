import { Article } from "$types/category";
import ArticleCard from "../ArticleCard";
import { StyledCardListContainer } from "./styled";

type ArticleListProps = {
    articles: Article[];
}

function ArticleList(props: ArticleListProps) {
    const { articles } = props;
    return (
        <StyledCardListContainer>
            {articles.map((article) => (
                <ArticleCard
                    key={`${article.name}-${article.prices.regular.value}`} 
                    article={article}
                />
            ))}
        </StyledCardListContainer>
    )
}

export default ArticleList;
