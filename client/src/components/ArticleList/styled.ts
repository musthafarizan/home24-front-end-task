import styled from "@emotion/styled";

export const StyledCardListContainer = styled.div`
    padding: 40px 20px;
    display: flex;
    flex-wrap: wrap;
    justify-content: start;
    align-items: stretch;
`