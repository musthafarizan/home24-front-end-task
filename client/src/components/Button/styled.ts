import styled from "@emotion/styled";
import { theme } from "src/styles/theme";

export const StyledButton = styled.button`
    width: 100%;
    padding: 8px;
    outline: none;
    border: none;
    background-color: ${theme.colors.primary.main};
    color: ${theme.colors.grey.dark};
    cursor: pointer;
    :hover {
        background-color: ${theme.colors.primary.dark};
        color: ${theme.colors.grey.light};
    }
`