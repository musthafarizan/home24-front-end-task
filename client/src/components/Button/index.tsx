import { StyledButton } from "./styled";

type ButtonProps = {
    label: string;

}

function Button(props: ButtonProps) {
    const {label} = props
    return (
        <StyledButton>
            {label}
        </StyledButton>
    )
}

export default Button;
