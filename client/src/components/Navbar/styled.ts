import styled from "@emotion/styled";
import { theme } from "src/styles/theme";

export const StyledNav = styled.nav`
    display: flex;
    position: sticky;
    justify-content: space-between;
    padding: 10px 20px;
    background-color: ${theme.colors.grey.light};
    box-shadow: 0 5px 5px ${theme.colors.grey.dark};
    align-items: center;
    a {
        padding: 5px 10px;
    }
`

export const StyledNavTitle = styled.a`
    display: inline-block;
    img {
        height: 25px;
    }
`

export const StyledNavList = styled.div`
    display: flex;
    gap: 0.5rem;
    :hover {
        color: ${theme.colors.black.dark};
    }
`