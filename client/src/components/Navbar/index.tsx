import { mainLogo } from "src/assets/images";
import { StyledNav, StyledNavList, StyledNavTitle } from "./styled";

function Navbar() {
    return (
        <StyledNav>
            <h3>
                <StyledNavTitle href="/">
                    <img src={mainLogo} alt="main logo" />
                </StyledNavTitle>
            </h3>
            <StyledNavList>
                <a href="/signin">Sign In</a>
                <a href="/register">Register</a>
            </StyledNavList>
        </StyledNav>
    );
}

export default Navbar;
