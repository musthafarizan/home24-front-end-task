import styled from "@emotion/styled";
import Lottie from 'lottie-react';
import { theme } from "src/styles/theme";

export const StyledLoaderContainer = styled.div`
    position: absolute;
    width: 100%;
    height: 100%;
    top: 0;
    right: 0;
    display: flex;
    justify-content: center;
    align-items: center;
    background-color: ${theme.colors.grey.dark + 'aa'};
`

export const StyledLottie = styled(Lottie)`
    max-width: 120px;
    max-height: 120px;
`