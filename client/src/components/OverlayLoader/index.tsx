import { loadingAnimation } from 'src/assets/images';
import { StyledLoaderContainer, StyledLottie } from './styled';

function OverlayLoader() {
    return (
        <StyledLoaderContainer>
            <StyledLottie animationData={loadingAnimation} loop />
        </StyledLoaderContainer>
    )
}

export default OverlayLoader;
