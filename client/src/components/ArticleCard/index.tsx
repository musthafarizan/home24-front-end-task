import { Article } from "$types/category";
import { currencyDisplayFormat } from "src/helpers/currencyFormatter";
import Button from "../Button";
import { StyledCardContainer, StyledCardDescriptionP, StyledCardDetailsContainer, StyledCardImageContainer, StyledCardInnerContainer, StyledPriceTag } from "./styled";

type ArticleCardProps = {
    article: Article
}

function ArticleCard(props: ArticleCardProps) {
    const { article } = props;
    return (
        <StyledCardContainer>
            <StyledCardInnerContainer>
                <StyledCardImageContainer imgUrl={article.images[0].path} />
                <StyledCardDetailsContainer>
                    <StyledPriceTag>
                        {article.prices.currency} {currencyDisplayFormat(article.prices.regular.value)}
                    </StyledPriceTag>
                    <h4>{article.name}</h4>
                    <StyledCardDescriptionP>
                        {article.variantName}
                    </StyledCardDescriptionP>
                </StyledCardDetailsContainer>
                <Button label="Add to Cart" />
            </StyledCardInnerContainer>
        </StyledCardContainer>
    )
}

export default ArticleCard;
