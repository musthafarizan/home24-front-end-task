import styled from "@emotion/styled";
import { theme } from "src/styles/theme";

export const StyledCardContainer = styled.article`
    padding: 12px;

    @media only screen and (${theme.devices.xs}) {
        width: 100%;
    }
    @media only screen and (${theme.devices.sm}) {
        width: 50%;
    }
    @media only screen and (${theme.devices.md}) {
        width: 33.33%;
    }
    @media only screen and (${theme.devices.lg}) {
        width: 25%;
    }
    @media only screen and (${theme.devices.xl}) {
        width: 20%;
    }
`

export const StyledCardInnerContainer = styled.div`
    width: 100%;
    height: 100%;
    background-color: ${theme.colors.grey.light};
    border-radius: 10px;
    box-shadow: 2px 2px 5px ${theme.colors.grey.dark};
    display: flex;
    flex-direction: column;
`

export const StyledCardImageContainer = styled.div<{imgUrl: string}>`
    width: 100%;
    height: 200px;
    overflow: hidden;
    background: ${props => `url(${props.imgUrl})`};
    background-position: center;
    background-repeat: no-repeat;
    background-size: contain;
`

export const StyledCardDetailsContainer = styled.div`
    padding: 12px;
    flex: 1;
`

export const StyledPriceTag = styled.p`
    font-size: 1.2rem;
    color: ${theme.colors.primary.main};
`

export const StyledCardDescriptionP = styled.p`
    padding-top: 6px;
    font-size: 0.8rem;
    color: ${theme.colors.black.light};
`