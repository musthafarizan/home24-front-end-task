export function currencyDisplayFormat(amount: number) {
    const eurDEFormatter = new Intl.NumberFormat('de', {
        style: 'currency',
        currency: 'EUR',
    })

    return eurDEFormatter.format(amount);
}