import { css } from '@emotion/react'
import NunitoSansTTF from '../assets/fonts/NunitoSans_7pt-Regular.ttf'
import { theme } from './theme'

export default css`
    * {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
    }

    @font-face {
        font-family: NunitoSans;
        src: url(${NunitoSansTTF});
    }
    :root {
        font-family: NunitoSans;
        font-size: 14px;
    }

    body {
        background-color: ${theme.colors.grey.main};
    }

    #root {
        min-height: 100vh;
        display: flex;
        flex-direction: column;
    }

    a {
        text-decoration: none;
        color: inherit;
    }
`