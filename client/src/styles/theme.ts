import { MediaBreakPoints, Theme } from "$types/theme";

export const mediaBreakPointSizes: MediaBreakPoints = {
    xs: 320,
    sm: 640,
    md: 768,
    lg: 1024,
    xl: 1280,
    xxl: 1536,
} 

export const theme: Theme = {
    colors: {
        black: {
            dark: '#000000',
            light: '#222222',
            main: '#111111',
        },
        grey: {
            dark: '#f0f0f0',
            light: '#fdfdfd',
            main: '#fafafa',
        },
        primary: {
            dark: '#f43314',
            light: '#f49d34',
            main: '#f45334',
        },
    },
    devices: {
        xs: `min-width: ${mediaBreakPointSizes.xs}px`,
        sm: `min-width: ${mediaBreakPointSizes.sm}px`,
        md: `min-width: ${mediaBreakPointSizes.md}px`,
        lg: `min-width: ${mediaBreakPointSizes.lg}px`,
        xl: `min-width: ${mediaBreakPointSizes.xl}px`,
        xxl: `min-width: ${mediaBreakPointSizes.xxl}px`,
    }
}