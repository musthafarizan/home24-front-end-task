import { Category } from '$types/category';
import { useState, useEffect, useMemo } from 'react';
import categoryService from 'src/services/category.service';

export default function useCategoryState() {
    const [category, setCategory] = useState<Category>();
    const [loading, setLoading] = useState<boolean>(false);

    const articles = useMemo(() => category?.categoryArticles.articles, [category]);
  
    useEffect(
      () => {
        setLoading(true);
        categoryService.getAllById()
        .then(res => {
          setCategory(res?.data.categories[0]);
          setLoading(false);
        })
      },
      []
    );

    return { articles, category, loading }
}