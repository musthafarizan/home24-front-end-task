import mainLogo from './home-24-logo.svg';
import loadingAnimation from './loading-animation.json'

export {
    mainLogo,
    loadingAnimation,
}