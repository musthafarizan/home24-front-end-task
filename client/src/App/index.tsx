import { Global } from '@emotion/react';
import globalStyle from '../styles/globalStyle';
import Navbar from '../components/Navbar';
import OverlayLoader from '../components/OverlayLoader';
import { StyledDivider, StyledHeading, StyledMainContainer } from './styled';
import useCategoryState from 'src/hooks/useCategoryState';
import ArticleList from 'src/components/ArticleList';

function App() {
  const { category, articles, loading } = useCategoryState();

  return (
    <>
      <Global styles={globalStyle} />
      <>
        <Navbar />
        <StyledMainContainer>
          {loading && <OverlayLoader />}
          <StyledHeading>{category?.name}</StyledHeading>
          <StyledDivider />
          <ArticleList articles={articles || []} />
        </StyledMainContainer>
      </>
    </>
  );
}

export default App;
