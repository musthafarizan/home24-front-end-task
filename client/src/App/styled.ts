import styled from "@emotion/styled";
import { theme } from "src/styles/theme";

export const StyledMainContainer = styled.main`
    position: relative;
    flex: 1;
`

export const StyledHeading = styled.h2`
    font-size: 2.5rem;
    padding: 20px 40px 0;

`

export const StyledDivider = styled.hr`
    background-color: ${theme.colors.primary.main};
    margin: 0 40px;
    height: 2px;
    border: none;
`