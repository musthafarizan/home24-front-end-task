export default {
    "categories": [
      {
        "name": "Möbel",
        "categoryArticles": {
          "articles": [
            {
              "sku": "000000001000373887",
              "parentSku": "000000008000047367",
              "url": "produkt/esstisch-moato-keramik-metall-marmor-schwarz-dekor",
              "name": "Esstisch Moato",
              "trackingId": null,
              "images": [
                {
                  "path": "https://cdn1.home24.net/images/media/catalog/product/original/png/-/1/-1000373887-230516-010-IMAGE-P000000001000373887.webp"
                }
              ],
              "labels": [
                {
                  "type": "campaign",
                  "value": "EXTRA -10 %"
                },
                {
                  "type": "discount",
                  "value": "-28%"
                }
              ],
              "delivery": {
                "flags": {
                  "fastDelivery": true
                },
                "time": {
                  "numberOfDays": 10
                }
              },
              "prices": {
                "regular": {
                  "value": 51999,
                  "isRrp": false
                },
                "special": {
                  "value": 36999,
                  "discount": 15000
                }
              },
              "sellable": true,
              "ratings": {
                "count": 0,
                "average": 0
              },
              "brand": {
                "name": "Studio Copenhagen"
              },
              "breadcrumbs": [
                {
                  "name": "Startseite"
                },
                {
                  "name": "Möbel"
                },
                {
                  "name": "Tische"
                },
                {
                  "name": "Esstische"
                }
              ],
              "campaigns": [
                {
                  "key": "xhcgspx-backtoschool10-0723"
                },
                {
                  "key": "backtoschool23"
                },
                {
                  "key": "xhcgspx-flash12de-0823"
                },
                {
                  "key": "backtoschool-sku-23"
                },
                {
                  "key": "salespecial"
                },
                {
                  "key": "sale20"
                },
                {
                  "key": "h24campaigns"
                },
                {
                  "key": "influencer"
                },
                {
                  "key": "nonspecial-nonmp"
                },
                {
                  "key": "sale"
                },
                {
                  "key": "24series"
                },
                {
                  "key": "meinestadt"
                },
                {
                  "key": "nonmp"
                },
                {
                  "key": "reduced-assortment"
                }
              ],
              "variantName": "Keramik / Metall - Marmor Schwarz Dekor",
              "energyClass": null,
              "sponsored": null,
              "variations": [
                {
                  "renderingType": "icon",
                  "key": "colorSubcolor",
                  "title": "Farbe",
                  "groups": [
                    {
                      "values": [
                        {
                          "sku": "000000001000373887",
                          "imageUrl": "/subcolor-4160-421352.png",
                          "selected": true
                        },
                        {
                          "sku": "000000001000373888",
                          "imageUrl": "/subcolor-4158-421748.png",
                          "selected": false
                        }
                      ]
                    }
                  ]
                }
              ],
              "taxonomyBreadcrumbs": [
                {
                  "name": "Startseite",
                  "url": "/"
                },
                {
                  "name": "Möbel",
                  "url": "moebel-sortiment/"
                },
                {
                  "name": "Tische",
                  "url": "alle-tische/"
                },
                {
                  "name": "Esstische",
                  "url": "alle-esstische/"
                }
              ]
            },
            {
              "sku": "000000003000219909",
              "parentSku": "000000008500040825",
              "url": "produkt/matratze-140x200-kaltschaum-h3-h2-140-x-200-cm",
              "name": "Matratze 140x200 Kaltschaum H3 H2",
              "trackingId": null,
              "images": [
                {
                  "path": "https://cdn1.home24.net/images/media/catalog/product/original/png/c/6/c6b4b8e0aa814e2c88a6a1fbae52d892.webp"
                }
              ],
              "labels": [
                {
                  "type": "discount",
                  "value": "-56%"
                }
              ],
              "delivery": {
                "flags": {
                  "fastDelivery": true
                },
                "time": {
                  "numberOfDays": 5
                }
              },
              "prices": {
                "regular": {
                  "value": 29990,
                  "isRrp": false
                },
                "special": {
                  "value": 12990,
                  "discount": 17000
                }
              },
              "sellable": true,
              "ratings": {
                "count": 0,
                "average": 0
              },
              "brand": {
                "name": "Mister Sandman"
              },
              "breadcrumbs": [
                {
                  "name": "Startseite"
                },
                {
                  "name": "Möbel"
                },
                {
                  "name": "Matratzen"
                },
                {
                  "name": "Kaltschaummatratzen"
                }
              ],
              "campaigns": [
                {
                  "key": "sale40"
                },
                {
                  "key": "MP-sale"
                },
                {
                  "key": "MP-special"
                }
              ],
              "variantName": "140 x 200 cm",
              "energyClass": null,
              "sponsored": null,
              "variations": [
                {
                  "renderingType": "text",
                  "key": "_width_depth",
                  "title": "Breite x Tiefe",
                  "groups": [
                    {
                      "values": [
                        {
                          "sku": "000000003000219895",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000003000219898",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000003000219892",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000003000219883",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000003000219884",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000003000219909",
                          "imageUrl": null,
                          "selected": true
                        }
                      ]
                    }
                  ]
                }
              ],
              "taxonomyBreadcrumbs": [
                {
                  "name": "Startseite",
                  "url": "/"
                },
                {
                  "name": "Möbel",
                  "url": "moebel-sortiment/"
                },
                {
                  "name": "Matratzen",
                  "url": "matratze/"
                },
                {
                  "name": "Kaltschaummatratzen",
                  "url": "kaltschaummatratze/"
                }
              ]
            },
            {
              "sku": "000000001000194670",
              "parentSku": "000000008000026905",
              "url": "produkt/drehtuerenschrank-diver-anthrazit-plankeneiche-dekor-breite-270-cm",
              "name": "Drehtürenschrank Diver",
              "trackingId": null,
              "images": [
                {
                  "path": "https://cdn1.home24.net/images/media/catalog/product/original/png/-/1/-1000194670-191202-11352600102-IMAGE-P000000001000194670.webp"
                }
              ],
              "labels": [
                {
                  "type": "campaign",
                  "value": "SPECIAL"
                }
              ],
              "delivery": {
                "flags": {
                  "fastDelivery": true
                },
                "time": {
                  "numberOfDays": 10
                }
              },
              "prices": {
                "regular": {
                  "value": 46999,
                  "isRrp": false
                },
                "special": null
              },
              "sellable": true,
              "ratings": {
                "count": 159,
                "average": 4.3
              },
              "brand": {
                "name": "Wimex"
              },
              "breadcrumbs": [
                {
                  "name": "Startseite"
                },
                {
                  "name": "Möbel"
                },
                {
                  "name": "Schränke"
                },
                {
                  "name": "Kleiderschränke"
                }
              ],
              "campaigns": [
                {
                  "key": "specialdeals"
                },
                {
                  "key": "nonmp"
                }
              ],
              "variantName": "Anthrazit / Plankeneiche Dekor - Breite: 270 cm",
              "energyClass": null,
              "sponsored": null,
              "variations": [
                {
                  "renderingType": "icon",
                  "key": "colorSubcolor",
                  "title": "Farbe",
                  "groups": [
                    {
                      "values": [
                        {
                          "sku": "000000001000194667",
                          "imageUrl": "/alpinwhitePlankoakDecor.png",
                          "selected": false
                        },
                        {
                          "sku": "000000001000194670",
                          "imageUrl": "/anthracitePlankoakDecor.png",
                          "selected": true
                        },
                        {
                          "sku": "000000001000194658",
                          "imageUrl": "/subcolor-4497-421697.png",
                          "selected": false
                        },
                        {
                          "sku": "000000001000194639",
                          "imageUrl": "/subcolor-4498-421698.png",
                          "selected": false
                        },
                        {
                          "sku": "000000001000194640",
                          "imageUrl": "/subcolor-4532-421737.png",
                          "selected": false
                        }
                      ]
                    }
                  ]
                },
                {
                  "renderingType": "text",
                  "key": "width",
                  "title": "Breite",
                  "groups": [
                    {
                      "values": [
                        {
                          "sku": "000000001000194676",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000001000194670",
                          "imageUrl": null,
                          "selected": true
                        }
                      ]
                    }
                  ]
                }
              ],
              "taxonomyBreadcrumbs": [
                {
                  "name": "Startseite",
                  "url": "/"
                },
                {
                  "name": "Möbel",
                  "url": "moebel-sortiment/"
                },
                {
                  "name": "Schränke",
                  "url": "schraenke-stauraum/"
                },
                {
                  "name": "Kleiderschränke",
                  "url": "alle-kleiderschraenke/"
                }
              ]
            },
            {
              "sku": "000000003000602455",
              "parentSku": "000000008500126082",
              "url": "produkt/bemmi-kommode-2-tuerig-dunkelgruen-aa21",
              "name": "BEMMI Kommode 2-türig",
              "trackingId": null,
              "images": [
                {
                  "path": "https://cdn1.home24.net/images/media/catalog/product/original/jpg/5/e/5e0a0f8572f946659ba51e05562a0c24.webp"
                }
              ],
              "labels": [
                {
                  "type": "new",
                  "value": "Neu"
                }
              ],
              "delivery": {
                "flags": {
                  "fastDelivery": false
                },
                "time": {
                  "numberOfDays": 32
                }
              },
              "prices": {
                "regular": {
                  "value": 15199,
                  "isRrp": false
                },
                "special": null
              },
              "sellable": true,
              "ratings": {
                "count": 0,
                "average": 0
              },
              "brand": {
                "name": "Selsey"
              },
              "breadcrumbs": [
                {
                  "name": "Startseite"
                },
                {
                  "name": "Möbel"
                },
                {
                  "name": "Sideboards"
                }
              ],
              "campaigns": [
                {
                  "key": "MP-special"
                }
              ],
              "variantName": "Dunkelgrün",
              "energyClass": null,
              "sponsored": null,
              "variations": [
                {
                  "renderingType": "icon",
                  "key": "colorSubcolor",
                  "title": "Farbe",
                  "groups": [
                    {
                      "values": [
                        {
                          "sku": "000000003000602455",
                          "imageUrl": "/subcolor-16498-893698.png",
                          "selected": true
                        },
                        {
                          "sku": "000000003000602456",
                          "imageUrl": "/subcolor-38544-3776060.png",
                          "selected": false
                        },
                        {
                          "sku": "000000003000602452",
                          "imageUrl": "/subcolor-4448-421633.png",
                          "selected": false
                        }
                      ]
                    }
                  ]
                }
              ],
              "taxonomyBreadcrumbs": [
                {
                  "name": "Startseite",
                  "url": "/"
                },
                {
                  "name": "Möbel",
                  "url": "moebel-sortiment/"
                },
                {
                  "name": "Sideboards",
                  "url": "alle-sideboards/"
                }
              ]
            },
            {
              "sku": "000000003000322632",
              "parentSku": "000000008500058978",
              "url": "produkt/riesen-sitzsack-sessel-morgan-cord-xxl-sitzsack-erwachsene-mit-fuellung-fuer-das-wohnzimmer-riesensitzsack-xxl-flauschig-cord-beige",
              "name": "Riesen Sitzsack Sessel Morgan",
              "trackingId": null,
              "images": [
                {
                  "path": "https://cdn1.home24.net/images/media/catalog/product/original/png/0/f/0f22982320164c2eade555918d6dabb1.cropped-0-377-2000-1592.processed.webp"
                }
              ],
              "labels": [],
              "delivery": {
                "flags": {
                  "fastDelivery": true
                },
                "time": {
                  "numberOfDays": 4
                }
              },
              "prices": {
                "regular": {
                  "value": 17999,
                  "isRrp": true
                },
                "special": null
              },
              "sellable": true,
              "ratings": {
                "count": 0,
                "average": 0
              },
              "brand": {
                "name": "Icon"
              },
              "breadcrumbs": [
                {
                  "name": "Startseite"
                },
                {
                  "name": "Möbel"
                },
                {
                  "name": "Sessel"
                },
                {
                  "name": "Sitzsäcke"
                }
              ],
              "campaigns": [
                {
                  "key": "MP-special"
                },
                {
                  "key": "bazaar"
                }
              ],
              "variantName": "Cord XXL Sitzsack Erwachsene mit Füllung für das Wohnzimmer, Riesensitzsack XXL Flauschig Cord - Beige",
              "energyClass": null,
              "sponsored": null,
              "variations": [
                {
                  "renderingType": "icon",
                  "key": "colorSubcolor",
                  "title": "Farbe",
                  "groups": [
                    {
                      "values": [
                        {
                          "sku": "000000003000322632",
                          "imageUrl": "/subcolor-4175-421323.png",
                          "selected": true
                        },
                        {
                          "sku": "000000003000322717",
                          "imageUrl": "/subcolor-4314-421473.png",
                          "selected": false
                        },
                        {
                          "sku": "000000003000322640",
                          "imageUrl": "/subcolor-4329-893750.png",
                          "selected": false
                        },
                        {
                          "sku": "000000003000322642",
                          "imageUrl": "/subcolor-4187-421585.png",
                          "selected": false
                        }
                      ]
                    }
                  ]
                }
              ],
              "taxonomyBreadcrumbs": [
                {
                  "name": "Startseite",
                  "url": "/"
                },
                {
                  "name": "Möbel",
                  "url": "moebel-sortiment/"
                },
                {
                  "name": "Sessel",
                  "url": "alle-sessel/"
                },
                {
                  "name": "Sitzsäcke",
                  "url": "alle-sitzsaecke/"
                }
              ]
            },
            {
              "sku": "000000001000365755",
              "parentSku": "000000008000047248",
              "url": "produkt/schwebetuerenschrank-tover-ii-weiss",
              "name": "Schwebetürenschrank Tover II",
              "trackingId": null,
              "images": [
                {
                  "path": "https://cdn1.home24.net/images/media/catalog/product/original/png/-/1/-1000365755-220901-010-IMAGE-P000000001000365755.webp"
                }
              ],
              "labels": [
                {
                  "type": "campaign",
                  "value": "EXTRA -12 %"
                }
              ],
              "delivery": {
                "flags": {
                  "fastDelivery": true
                },
                "time": {
                  "numberOfDays": 10
                }
              },
              "prices": {
                "regular": {
                  "value": 39999,
                  "isRrp": false
                },
                "special": null
              },
              "sellable": true,
              "ratings": {
                "count": 5,
                "average": 3.8
              },
              "brand": {
                "name": "loftscape"
              },
              "breadcrumbs": [
                {
                  "name": "Startseite"
                },
                {
                  "name": "Möbel"
                },
                {
                  "name": "Schränke"
                },
                {
                  "name": "Kleiderschränke"
                }
              ],
              "campaigns": [
                {
                  "key": "xhcgspx-flash12de-0823"
                },
                {
                  "key": "specialdeals"
                },
                {
                  "key": "h24campaigns"
                },
                {
                  "key": "nonmp"
                }
              ],
              "variantName": "Weiß",
              "energyClass": null,
              "sponsored": null,
              "variations": null,
              "taxonomyBreadcrumbs": [
                {
                  "name": "Startseite",
                  "url": "/"
                },
                {
                  "name": "Möbel",
                  "url": "moebel-sortiment/"
                },
                {
                  "name": "Schränke",
                  "url": "schraenke-stauraum/"
                },
                {
                  "name": "Kleiderschränke",
                  "url": "alle-kleiderschraenke/"
                }
              ]
            },
            {
              "sku": "000000001000136118",
              "parentSku": "000000008000018478",
              "url": "produkt/drehtuerenschrank-freiham-alpinweiss-alpinweiss-breite-136-cm-ohne-spiegeltuer-en",
              "name": "Drehtürenschrank Freiham",
              "trackingId": null,
              "images": [
                {
                  "path": "https://cdn1.home24.net/images/media/catalog/product/original/png/-/1/-1000136118-181026-12402010-IMAGE-P000000001000136118.webp"
                }
              ],
              "labels": [
                {
                  "type": "campaign",
                  "value": "SPECIAL"
                }
              ],
              "delivery": {
                "flags": {
                  "fastDelivery": true
                },
                "time": {
                  "numberOfDays": 10
                }
              },
              "prices": {
                "regular": {
                  "value": 27999,
                  "isRrp": false
                },
                "special": null
              },
              "sellable": true,
              "ratings": {
                "count": 261,
                "average": 4.4
              },
              "brand": {
                "name": "rauch Blue"
              },
              "breadcrumbs": [
                {
                  "name": "Startseite"
                },
                {
                  "name": "Möbel"
                },
                {
                  "name": "Schränke"
                },
                {
                  "name": "Kleiderschränke"
                }
              ],
              "campaigns": [
                {
                  "key": "specialdeals"
                },
                {
                  "key": "firstflat"
                },
                {
                  "key": "sustainable"
                },
                {
                  "key": "nonmp"
                }
              ],
              "variantName": "Alpinweiß - Alpinweiß - Breite: 136 cm - Ohne Spiegeltür/-en",
              "energyClass": null,
              "sponsored": null,
              "variations": [
                {
                  "renderingType": "text",
                  "key": "width",
                  "title": "Breite",
                  "groups": [
                    {
                      "values": [
                        {
                          "sku": "000000001000136117",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000001000136129",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000001000136118",
                          "imageUrl": null,
                          "selected": true
                        },
                        {
                          "sku": "000000001000136132",
                          "imageUrl": null,
                          "selected": false
                        }
                      ]
                    }
                  ]
                },
                {
                  "renderingType": "text",
                  "key": "cabinetMirrors",
                  "title": "Anzahl Spiegeltüren",
                  "groups": [
                    {
                      "values": [
                        {
                          "sku": "000000001000136130",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000001000136118",
                          "imageUrl": null,
                          "selected": true
                        }
                      ]
                    }
                  ]
                }
              ],
              "taxonomyBreadcrumbs": [
                {
                  "name": "Startseite",
                  "url": "/"
                },
                {
                  "name": "Möbel",
                  "url": "moebel-sortiment/"
                },
                {
                  "name": "Schränke",
                  "url": "schraenke-stauraum/"
                },
                {
                  "name": "Kleiderschränke",
                  "url": "alle-kleiderschraenke/"
                }
              ]
            },
            {
              "sku": "000000003000219988",
              "parentSku": "000000008500040837",
              "url": "produkt/orthopaedische-matratze-140x200-gelschaum-breite-140-cm",
              "name": "Orthopädische matratze 140x200 Gelschaum",
              "trackingId": null,
              "images": [
                {
                  "path": "https://cdn1.home24.net/images/media/catalog/product/original/png/9/9/991883cc43934b168988b58676300b95.cropped-7-406-2026-1372.processed.webp"
                }
              ],
              "labels": [],
              "delivery": {
                "flags": {
                  "fastDelivery": true
                },
                "time": {
                  "numberOfDays": 5
                }
              },
              "prices": {
                "regular": {
                  "value": 13990,
                  "isRrp": false
                },
                "special": null
              },
              "sellable": true,
              "ratings": {
                "count": 0,
                "average": 0
              },
              "brand": {
                "name": "Mister Sandman"
              },
              "breadcrumbs": [
                {
                  "name": "Startseite"
                },
                {
                  "name": "Möbel"
                },
                {
                  "name": "Matratzen"
                },
                {
                  "name": "Gelmatratzen"
                }
              ],
              "campaigns": [
                {
                  "key": "MP-special"
                }
              ],
              "variantName": "Breite: 140 cm",
              "energyClass": null,
              "sponsored": null,
              "variations": [
                {
                  "renderingType": "text",
                  "key": "width",
                  "title": "Breite",
                  "groups": [
                    {
                      "values": [
                        {
                          "sku": "000000003000219965",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000003000219981",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000003000219954",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000003000219988",
                          "imageUrl": null,
                          "selected": true
                        },
                        {
                          "sku": "000000003000219967",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000003000219940",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000003000219923",
                          "imageUrl": null,
                          "selected": false
                        }
                      ]
                    }
                  ]
                }
              ],
              "taxonomyBreadcrumbs": [
                {
                  "name": "Startseite",
                  "url": "/"
                },
                {
                  "name": "Möbel",
                  "url": "moebel-sortiment/"
                },
                {
                  "name": "Matratzen",
                  "url": "matratze/"
                },
                {
                  "name": "Gelmatratzen",
                  "url": "gelmatratzen/"
                }
              ]
            },
            {
              "sku": "000000003000602423",
              "parentSku": "000000008500125997",
              "url": "produkt/bemmi-tv-schrank-dunkelgruen-c637",
              "name": "BEMMI TV-Schrank",
              "trackingId": null,
              "images": [
                {
                  "path": "https://cdn1.home24.net/images/media/catalog/product/original/jpg/0/1/017e945fd4ae49f680881307250d3b9f.webp"
                }
              ],
              "labels": [
                {
                  "type": "new",
                  "value": "Neu"
                }
              ],
              "delivery": {
                "flags": {
                  "fastDelivery": false
                },
                "time": {
                  "numberOfDays": 32
                }
              },
              "prices": {
                "regular": {
                  "value": 10699,
                  "isRrp": false
                },
                "special": null
              },
              "sellable": true,
              "ratings": {
                "count": 0,
                "average": 0
              },
              "brand": {
                "name": "Selsey"
              },
              "breadcrumbs": [
                {
                  "name": "Startseite"
                },
                {
                  "name": "Möbel"
                },
                {
                  "name": "TV-Möbel"
                },
                {
                  "name": "TV-Lowboards"
                }
              ],
              "campaigns": [
                {
                  "key": "MP-special"
                }
              ],
              "variantName": "Dunkelgrün",
              "energyClass": null,
              "sponsored": null,
              "variations": [
                {
                  "renderingType": "icon",
                  "key": "colorSubcolor",
                  "title": "Farbe",
                  "groups": [
                    {
                      "values": [
                        {
                          "sku": "000000003000602423",
                          "imageUrl": "/subcolor-16498-893698.png",
                          "selected": true
                        },
                        {
                          "sku": "000000003000602435",
                          "imageUrl": "/subcolor-38544-3776060.png",
                          "selected": false
                        }
                      ]
                    }
                  ]
                }
              ],
              "taxonomyBreadcrumbs": [
                {
                  "name": "Startseite",
                  "url": "/"
                },
                {
                  "name": "Möbel",
                  "url": "moebel-sortiment/"
                },
                {
                  "name": "TV-Möbel",
                  "url": "fernseh-tv-moebel/"
                },
                {
                  "name": "TV-Lowboards",
                  "url": "tv-lowboards/"
                }
              ]
            },
            {
              "sku": "000000003000579882",
              "parentSku": "000000008500119447",
              "url": "produkt/blossom-xl-hocker-mit-aufbewahrung-weiss",
              "name": "BLOSSOM XL Hocker mit Aufbewahrung",
              "trackingId": null,
              "images": [
                {
                  "path": "https://cdn1.home24.net/images/media/catalog/product/original/png/2/3/237bf76a6a3048d294fc6251025f3a5a.cropped-154-310-1353-1058.processed.webp"
                }
              ],
              "labels": [
                {
                  "type": "new",
                  "value": "Neu"
                }
              ],
              "delivery": {
                "flags": {
                  "fastDelivery": true
                },
                "time": {
                  "numberOfDays": 6
                }
              },
              "prices": {
                "regular": {
                  "value": 12499,
                  "isRrp": false
                },
                "special": null
              },
              "sellable": true,
              "ratings": {
                "count": 0,
                "average": 0
              },
              "brand": {
                "name": "Svita"
              },
              "breadcrumbs": [
                {
                  "name": "Startseite"
                },
                {
                  "name": "Möbel"
                },
                {
                  "name": "Hocker"
                },
                {
                  "name": "Sitzhocker"
                }
              ],
              "campaigns": [
                {
                  "key": "MP-special"
                }
              ],
              "variantName": "Weiß",
              "energyClass": null,
              "sponsored": null,
              "variations": [
                {
                  "renderingType": "icon",
                  "key": "colorSubcolor",
                  "title": "Farbe",
                  "groups": [
                    {
                      "values": [
                        {
                          "sku": "000000003000579887",
                          "imageUrl": "/subcolor-4131-456008.png",
                          "selected": false
                        },
                        {
                          "sku": "000000003000579888",
                          "imageUrl": "/subcolor-4241-421371.png",
                          "selected": false
                        },
                        {
                          "sku": "000000003000579879",
                          "imageUrl": "/subcolor-8215-421524.png",
                          "selected": false
                        },
                        {
                          "sku": "000000003000579882",
                          "imageUrl": "/subcolor-4097-421717.png",
                          "selected": true
                        }
                      ]
                    }
                  ]
                }
              ],
              "taxonomyBreadcrumbs": [
                {
                  "name": "Startseite",
                  "url": "/"
                },
                {
                  "name": "Möbel",
                  "url": "moebel-sortiment/"
                },
                {
                  "name": "Hocker",
                  "url": "alle-hocker/"
                },
                {
                  "name": "Sitzhocker",
                  "url": "sitzhocker/"
                }
              ]
            },
            {
              "sku": "000000003000382653",
              "parentSku": "000000008500076615",
              "url": "produkt/farese-3-sitzer-schlafsofa-selsey-farese-3-sitzer-schlafsofa-mit-bettzeugbehaelter-beige-beige",
              "name": "Farese 3-Sitzer Schlafsofa",
              "trackingId": null,
              "images": [
                {
                  "path": "https://cdn1.home24.net/images/media/catalog/product/original/jpg/6/d/6dad2e7920a44ceb8cf42dae463b56a6.webp"
                }
              ],
              "labels": [],
              "delivery": {
                "flags": {
                  "fastDelivery": false
                },
                "time": {
                  "numberOfDays": 19
                }
              },
              "prices": {
                "regular": {
                  "value": 81099,
                  "isRrp": false
                },
                "special": null
              },
              "sellable": true,
              "ratings": {
                "count": 1,
                "average": 5
              },
              "brand": {
                "name": "Selsey"
              },
              "breadcrumbs": [
                {
                  "name": "Startseite"
                },
                {
                  "name": "Möbel"
                },
                {
                  "name": "Sofas & Couches"
                },
                {
                  "name": "Einzelsofas"
                }
              ],
              "campaigns": [
                {
                  "key": "MP-special"
                }
              ],
              "variantName": "Selsey Farese 3-Sitzer Schlafsofa mit Bettzeugbehälter beige - Beige",
              "energyClass": null,
              "sponsored": null,
              "variations": [
                {
                  "renderingType": "icon",
                  "key": "colorSubcolor",
                  "title": "Farbe",
                  "groups": [
                    {
                      "values": [
                        {
                          "sku": "000000003000382653",
                          "imageUrl": "/subcolor-4175-421323.png",
                          "selected": true
                        },
                        {
                          "sku": "000000003000382652",
                          "imageUrl": "/subcolor-4234-421364.png",
                          "selected": false
                        },
                        {
                          "sku": "000000003000400360",
                          "imageUrl": "/subcolor-4284-421420.png",
                          "selected": false
                        },
                        {
                          "sku": "000000003000400361",
                          "imageUrl": "/subcolor-11093-491762.png",
                          "selected": false
                        }
                      ]
                    }
                  ]
                }
              ],
              "taxonomyBreadcrumbs": [
                {
                  "name": "Startseite",
                  "url": "/"
                },
                {
                  "name": "Möbel",
                  "url": "moebel-sortiment/"
                },
                {
                  "name": "Sofas & Couches",
                  "url": "sofa-couch/"
                },
                {
                  "name": "Einzelsofas",
                  "url": "einzelsofas/"
                }
              ]
            },
            {
              "sku": "000000003000492027",
              "parentSku": "000000008500104575",
              "url": "produkt/telire-tv-moebel-weiss-weiss",
              "name": "TELIRE TV-Möbel  weiß",
              "trackingId": null,
              "images": [
                {
                  "path": "https://cdn1.home24.net/images/media/catalog/product/original/jpg/7/7/77abcd00556d48f8a81170e88af49486.webp"
                }
              ],
              "labels": [
                {
                  "type": "new",
                  "value": "Neu"
                }
              ],
              "delivery": {
                "flags": {
                  "fastDelivery": true
                },
                "time": {
                  "numberOfDays": 4
                }
              },
              "prices": {
                "regular": {
                  "value": 13499,
                  "isRrp": false
                },
                "special": null
              },
              "sellable": true,
              "ratings": {
                "count": 0,
                "average": 0
              },
              "brand": {
                "name": "Selsey"
              },
              "breadcrumbs": [
                {
                  "name": "Startseite"
                },
                {
                  "name": "Möbel"
                },
                {
                  "name": "TV-Möbel"
                },
                {
                  "name": "TV-Lowboards"
                }
              ],
              "campaigns": [
                {
                  "key": "MP-special"
                }
              ],
              "variantName": "Weiß",
              "energyClass": null,
              "sponsored": null,
              "variations": [
                {
                  "renderingType": "icon",
                  "key": "colorSubcolor",
                  "title": "Farbe",
                  "groups": [
                    {
                      "values": [
                        {
                          "sku": "000000003000492035",
                          "imageUrl": "/graphite.png",
                          "selected": false
                        },
                        {
                          "sku": "000000003000492027",
                          "imageUrl": "/subcolor-4097-421717.png",
                          "selected": true
                        }
                      ]
                    }
                  ]
                }
              ],
              "taxonomyBreadcrumbs": [
                {
                  "name": "Startseite",
                  "url": "/"
                },
                {
                  "name": "Möbel",
                  "url": "moebel-sortiment/"
                },
                {
                  "name": "TV-Möbel",
                  "url": "fernseh-tv-moebel/"
                },
                {
                  "name": "TV-Lowboards",
                  "url": "tv-lowboards/"
                }
              ]
            },
            {
              "sku": "000000003000028561",
              "parentSku": "000000003000028561-P",
              "url": "produkt/schuhschrank-fsr89-hg",
              "name": "Schuhschrank FSR89-HG",
              "trackingId": null,
              "images": [
                {
                  "path": "https://cdn1.home24.net/images/media/catalog/product/original/png/3/3/3327a081b31b4a4fabd1113cee89271d.cropped-362-156-911-1404.processed.webp"
                }
              ],
              "labels": [],
              "delivery": {
                "flags": {
                  "fastDelivery": true
                },
                "time": {
                  "numberOfDays": 5
                }
              },
              "prices": {
                "regular": {
                  "value": 9495,
                  "isRrp": false
                },
                "special": null
              },
              "sellable": true,
              "ratings": {
                "count": 0,
                "average": 0
              },
              "brand": {
                "name": "SoBuy"
              },
              "breadcrumbs": [
                {
                  "name": "Startseite"
                },
                {
                  "name": "Möbel"
                },
                {
                  "name": "Garderoben"
                },
                {
                  "name": "Schuhschränke"
                },
                {
                  "name": "Schuhkipper"
                }
              ],
              "campaigns": [
                {
                  "key": "MP-special"
                },
                {
                  "key": "sobuyhallway"
                }
              ],
              "variantName": "",
              "energyClass": null,
              "sponsored": null,
              "variations": null,
              "taxonomyBreadcrumbs": [
                {
                  "name": "Startseite",
                  "url": "/"
                },
                {
                  "name": "Möbel",
                  "url": "moebel-sortiment/"
                },
                {
                  "name": "Garderoben",
                  "url": "flur-garderoben/"
                },
                {
                  "name": "Schuhschränke",
                  "url": "schuhschraenke/"
                },
                {
                  "name": "Schuhkipper",
                  "url": "schuhkipper/"
                }
              ]
            },
            {
              "sku": "000000003000318531",
              "parentSku": "000000008500056645",
              "url": "produkt/besenschrank-mehrzweckschrank-schrank-ms1-3w-bs-weiss",
              "name": "Besenschrank Mehrzweckschrank Schrank",
              "trackingId": null,
              "images": [
                {
                  "path": "https://cdn1.home24.net/images/media/catalog/product/original/jpg/3/8/38b58c17af2d42e6839139a050101e83.webp"
                }
              ],
              "labels": [],
              "delivery": {
                "flags": {
                  "fastDelivery": true
                },
                "time": {
                  "numberOfDays": 4
                }
              },
              "prices": {
                "regular": {
                  "value": 15990,
                  "isRrp": false
                },
                "special": null
              },
              "sellable": true,
              "ratings": {
                "count": 1,
                "average": 3
              },
              "brand": {
                "name": "mokebo"
              },
              "breadcrumbs": [
                {
                  "name": "Startseite"
                },
                {
                  "name": "Möbel"
                },
                {
                  "name": "Schränke"
                },
                {
                  "name": "Mehrzweckschränke"
                }
              ],
              "campaigns": [
                {
                  "key": "MP-special"
                }
              ],
              "variantName": "MS1.3w-bs - Weiß",
              "energyClass": null,
              "sponsored": null,
              "variations": [
                {
                  "renderingType": "icon",
                  "key": "colorSubcolor",
                  "title": "Farbe",
                  "groups": [
                    {
                      "values": [
                        {
                          "sku": "000000003000318542",
                          "imageUrl": "/subcolor-46292-4580304.png",
                          "selected": false
                        },
                        {
                          "sku": "000000003000318535",
                          "imageUrl": "/subcolor-4109-421670.png",
                          "selected": false
                        },
                        {
                          "sku": "000000003000318538",
                          "imageUrl": "/subcolor-4194-421333.png",
                          "selected": false
                        },
                        {
                          "sku": "000000003000318531",
                          "imageUrl": "/subcolor-4097-421717.png",
                          "selected": true
                        }
                      ]
                    }
                  ]
                }
              ],
              "taxonomyBreadcrumbs": [
                {
                  "name": "Startseite",
                  "url": "/"
                },
                {
                  "name": "Möbel",
                  "url": "moebel-sortiment/"
                },
                {
                  "name": "Schränke",
                  "url": "schraenke-stauraum/"
                },
                {
                  "name": "Mehrzweckschränke",
                  "url": "mehrzweckschraenke/"
                }
              ]
            },
            {
              "sku": "000000001000021718",
              "parentSku": "000000008000001863",
              "url": "produkt/ecksofa-westwell-mit-schlaffunktion-microfaser-bobil-dunkelgrau",
              "name": "Ecksofa Westwell (mit Schlaffunktion)",
              "trackingId": null,
              "images": [
                {
                  "path": "https://cdn1.home24.net/images/media/catalog/product/original/png/-/1/-1000021718-210913-16352500043-IMAGE-P000000001000021718.webp"
                }
              ],
              "labels": [
                {
                  "type": "campaign",
                  "value": "SPECIAL"
                }
              ],
              "delivery": {
                "flags": {
                  "fastDelivery": true
                },
                "time": {
                  "numberOfDays": 10
                }
              },
              "prices": {
                "regular": {
                  "value": 92999,
                  "isRrp": false
                },
                "special": null
              },
              "sellable": true,
              "ratings": {
                "count": 249,
                "average": 4.2
              },
              "brand": {
                "name": "Fredriks"
              },
              "breadcrumbs": [
                {
                  "name": "Startseite"
                },
                {
                  "name": "Möbel"
                },
                {
                  "name": "Sofas & Couches"
                },
                {
                  "name": "Ecksofas"
                }
              ],
              "campaigns": [
                {
                  "key": "specialdeals"
                },
                {
                  "key": "nonmp"
                }
              ],
              "variantName": "Microfaser Bobil: Dunkelgrau",
              "energyClass": null,
              "sponsored": null,
              "variations": [
                {
                  "renderingType": "icon",
                  "key": "fabric",
                  "title": "Material",
                  "groups": [
                    {
                      "values": [
                        {
                          "sku": "000000001000021718",
                          "imageUrl": "/textileBobilDarkgrey.png",
                          "selected": true
                        },
                        {
                          "sku": "000000001000021698",
                          "imageUrl": "/textileBobilGranite.png",
                          "selected": false
                        }
                      ]
                    }
                  ]
                }
              ],
              "taxonomyBreadcrumbs": [
                {
                  "name": "Startseite",
                  "url": "/"
                },
                {
                  "name": "Möbel",
                  "url": "moebel-sortiment/"
                },
                {
                  "name": "Sofas & Couches",
                  "url": "sofa-couch/"
                },
                {
                  "name": "Ecksofas",
                  "url": "eckcouches/"
                }
              ]
            },
            {
              "sku": "000000003000040924",
              "parentSku": "000000008500005152",
              "url": "produkt/tv-lowboard-haengend-damla-weiss",
              "name": "TV Lowboard Hängend Damla Weiß",
              "trackingId": null,
              "images": [
                {
                  "path": "https://cdn1.home24.net/images/media/catalog/product/original/jpg/e/6/e6260372ae074ddb938026a66ac93bbb.webp"
                }
              ],
              "labels": [],
              "delivery": {
                "flags": {
                  "fastDelivery": true
                },
                "time": {
                  "numberOfDays": 4
                }
              },
              "prices": {
                "regular": {
                  "value": 17990,
                  "isRrp": false
                },
                "special": null
              },
              "sellable": true,
              "ratings": {
                "count": 0,
                "average": 0
              },
              "brand": {
                "name": "Moebel17"
              },
              "breadcrumbs": [
                {
                  "name": "Startseite"
                },
                {
                  "name": "Möbel"
                },
                {
                  "name": "TV-Möbel"
                },
                {
                  "name": "TV-Lowboards"
                }
              ],
              "campaigns": [
                {
                  "key": "MP-special"
                }
              ],
              "variantName": "",
              "energyClass": null,
              "sponsored": null,
              "variations": null,
              "taxonomyBreadcrumbs": [
                {
                  "name": "Startseite",
                  "url": "/"
                },
                {
                  "name": "Möbel",
                  "url": "moebel-sortiment/"
                },
                {
                  "name": "TV-Möbel",
                  "url": "fernseh-tv-moebel/"
                },
                {
                  "name": "TV-Lowboards",
                  "url": "tv-lowboards/"
                }
              ]
            },
            {
              "sku": "000000001000361038",
              "parentSku": "000000008000046420",
              "url": "produkt/schwebetuerenschrank-helmond-weiss",
              "name": "Schwebetürenschrank Helmond",
              "trackingId": null,
              "images": [
                {
                  "path": "https://cdn1.home24.net/images/media/catalog/product/original/png/-/1/-1000361038-220803-010-IMAGE-P000000001000361038.webp"
                }
              ],
              "labels": [
                {
                  "type": "campaign",
                  "value": "SPECIAL"
                }
              ],
              "delivery": {
                "flags": {
                  "fastDelivery": true
                },
                "time": {
                  "numberOfDays": 10
                }
              },
              "prices": {
                "regular": {
                  "value": 27999,
                  "isRrp": false
                },
                "special": null
              },
              "sellable": true,
              "ratings": {
                "count": 9,
                "average": 3.6
              },
              "brand": {
                "name": "loftscape"
              },
              "breadcrumbs": [
                {
                  "name": "Startseite"
                },
                {
                  "name": "Möbel"
                },
                {
                  "name": "Schränke"
                },
                {
                  "name": "Kleiderschränke"
                }
              ],
              "campaigns": [
                {
                  "key": "specialdeals"
                },
                {
                  "key": "firstflat"
                },
                {
                  "key": "nonmp"
                }
              ],
              "variantName": "Weiß",
              "energyClass": null,
              "sponsored": null,
              "variations": [
                {
                  "renderingType": "icon",
                  "key": "colorSubcolor",
                  "title": "Farbe",
                  "groups": [
                    {
                      "values": [
                        {
                          "sku": "000000001000361000",
                          "imageUrl": "/subcolor-4109-421670.png",
                          "selected": false
                        },
                        {
                          "sku": "000000001000361038",
                          "imageUrl": "/subcolor-4097-421717.png",
                          "selected": true
                        }
                      ]
                    }
                  ]
                }
              ],
              "taxonomyBreadcrumbs": [
                {
                  "name": "Startseite",
                  "url": "/"
                },
                {
                  "name": "Möbel",
                  "url": "moebel-sortiment/"
                },
                {
                  "name": "Schränke",
                  "url": "schraenke-stauraum/"
                },
                {
                  "name": "Kleiderschränke",
                  "url": "alle-kleiderschraenke/"
                }
              ]
            },
            {
              "sku": "000000003000028569",
              "parentSku": "000000003000028569-P",
              "url": "produkt/schuhschrank-fsr100-w",
              "name": "Schuhschrank FSR100-W",
              "trackingId": null,
              "images": [
                {
                  "path": "https://cdn1.home24.net/images/media/catalog/product/original/png/c/a/ca1afe5060354eaa903b079b10b1a4b3.cropped-176-125-1151-1390.processed.webp"
                }
              ],
              "labels": [
                {
                  "type": "discount",
                  "value": "-7%"
                }
              ],
              "delivery": {
                "flags": {
                  "fastDelivery": true
                },
                "time": {
                  "numberOfDays": 5
                }
              },
              "prices": {
                "regular": {
                  "value": 12995,
                  "isRrp": false
                },
                "special": {
                  "value": 11995,
                  "discount": 1000
                }
              },
              "sellable": true,
              "ratings": {
                "count": 1,
                "average": 4
              },
              "brand": {
                "name": "SoBuy"
              },
              "breadcrumbs": [
                {
                  "name": "Startseite"
                },
                {
                  "name": "Möbel"
                },
                {
                  "name": "Garderoben"
                },
                {
                  "name": "Schuhschränke"
                },
                {
                  "name": "Schuhkipper"
                }
              ],
              "campaigns": [
                {
                  "key": "sale10"
                },
                {
                  "key": "MP-sale"
                },
                {
                  "key": "MP-special"
                },
                {
                  "key": "sobuyhallway"
                }
              ],
              "variantName": "",
              "energyClass": null,
              "sponsored": null,
              "variations": null,
              "taxonomyBreadcrumbs": [
                {
                  "name": "Startseite",
                  "url": "/"
                },
                {
                  "name": "Möbel",
                  "url": "moebel-sortiment/"
                },
                {
                  "name": "Garderoben",
                  "url": "flur-garderoben/"
                },
                {
                  "name": "Schuhschränke",
                  "url": "schuhschraenke/"
                },
                {
                  "name": "Schuhkipper",
                  "url": "schuhkipper/"
                }
              ]
            },
            {
              "sku": "000000001000136095",
              "parentSku": "000000001000136095-P",
              "url": "produkt/wohnwand-lewk-iii-4-teilig-pinie-weiss-dekor-pinie-dekor",
              "name": "Wohnwand Lewk III (4-teilig)",
              "trackingId": null,
              "images": [
                {
                  "path": "https://cdn1.home24.net/images/media/catalog/product/original/png/-/1/-1000136095-190409-15533200010-IMAGE-P000000001000136095.webp"
                }
              ],
              "labels": [],
              "delivery": {
                "flags": {
                  "fastDelivery": true
                },
                "time": {
                  "numberOfDays": 10
                }
              },
              "prices": {
                "regular": {
                  "value": 91999,
                  "isRrp": false
                },
                "special": null
              },
              "sellable": true,
              "ratings": {
                "count": 18,
                "average": 4.3
              },
              "brand": {
                "name": "Ridgevalley"
              },
              "breadcrumbs": [
                {
                  "name": "Startseite"
                },
                {
                  "name": "Möbel"
                },
                {
                  "name": "Wohnwände"
                }
              ],
              "campaigns": [
                {
                  "key": "influencer"
                },
                {
                  "key": "nonspecial-mp-reduced"
                },
                {
                  "key": "nonspecial-nonmp"
                },
                {
                  "key": "xdgspx-googleshopping"
                },
                {
                  "key": "meinestadt"
                },
                {
                  "key": "nonmp"
                }
              ],
              "variantName": "Pinie Weiß Dekor / Pinie Dekor",
              "energyClass": null,
              "sponsored": null,
              "variations": null,
              "taxonomyBreadcrumbs": [
                {
                  "name": "Startseite",
                  "url": "/"
                },
                {
                  "name": "Möbel",
                  "url": "moebel-sortiment/"
                },
                {
                  "name": "Wohnwände",
                  "url": "wohnwand/"
                }
              ]
            },
            {
              "sku": "000000001000076760",
              "parentSku": "000000008000008356",
              "url": "produkt/drehtuerenschrank-passau-breite-271-cm",
              "name": "Drehtürenschrank Passau",
              "trackingId": null,
              "images": [
                {
                  "path": "https://cdn1.home24.net/images/media/catalog/product/original/png/d/r/drehtuerenschrank-passau-271-cm-6-tuerig-4814732.webp"
                }
              ],
              "labels": [
                {
                  "type": "campaign",
                  "value": "EXTRA -12 %"
                }
              ],
              "delivery": {
                "flags": {
                  "fastDelivery": true
                },
                "time": {
                  "numberOfDays": 10
                }
              },
              "prices": {
                "regular": {
                  "value": 64999,
                  "isRrp": false
                },
                "special": null
              },
              "sellable": true,
              "ratings": {
                "count": 66,
                "average": 4.3
              },
              "brand": {
                "name": "rauch Orange"
              },
              "breadcrumbs": [
                {
                  "name": "Startseite"
                },
                {
                  "name": "Möbel"
                },
                {
                  "name": "Schränke"
                },
                {
                  "name": "Kleiderschränke"
                }
              ],
              "campaigns": [
                {
                  "key": "xhcgspx-flash12de-0823"
                },
                {
                  "key": "h24campaigns"
                },
                {
                  "key": "influencer"
                },
                {
                  "key": "nonspecial-mp-reduced"
                },
                {
                  "key": "nonspecial-nonmp"
                },
                {
                  "key": "meinestadt"
                },
                {
                  "key": "sustainable"
                },
                {
                  "key": "nonmp"
                }
              ],
              "variantName": "Breite: 271 cm",
              "energyClass": null,
              "sponsored": null,
              "variations": [
                {
                  "renderingType": "text",
                  "key": "width",
                  "title": "Breite",
                  "groups": [
                    {
                      "values": [
                        {
                          "sku": "000000001000076758",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000001000076760",
                          "imageUrl": null,
                          "selected": true
                        }
                      ]
                    }
                  ]
                }
              ],
              "taxonomyBreadcrumbs": [
                {
                  "name": "Startseite",
                  "url": "/"
                },
                {
                  "name": "Möbel",
                  "url": "moebel-sortiment/"
                },
                {
                  "name": "Schränke",
                  "url": "schraenke-stauraum/"
                },
                {
                  "name": "Kleiderschränke",
                  "url": "alle-kleiderschraenke/"
                }
              ]
            },
            {
              "sku": "000000003000139078",
              "parentSku": "000000008500036113",
              "url": "produkt/cord-ecksofa-rouen-beige-ecke-davorstehend-links",
              "name": "Cord Ecksofa Rouen",
              "trackingId": null,
              "images": [
                {
                  "path": "https://cdn1.home24.net/images/media/catalog/product/original/jpg/5/c/5c9d3359916d42138f63174e8a45786f.webp"
                }
              ],
              "labels": [],
              "delivery": {
                "flags": {
                  "fastDelivery": true
                },
                "time": {
                  "numberOfDays": 7
                }
              },
              "prices": {
                "regular": {
                  "value": 89999,
                  "isRrp": false
                },
                "special": null
              },
              "sellable": true,
              "ratings": {
                "count": 4,
                "average": 5
              },
              "brand": {
                "name": "S-Style Möbel"
              },
              "breadcrumbs": [
                {
                  "name": "Startseite"
                },
                {
                  "name": "Möbel"
                },
                {
                  "name": "Sofas & Couches"
                },
                {
                  "name": "Ecksofas"
                }
              ],
              "campaigns": [
                {
                  "key": "MP-special"
                }
              ],
              "variantName": "Beige - Ecke davorstehend links",
              "energyClass": null,
              "sponsored": null,
              "variations": [
                {
                  "renderingType": "icon",
                  "key": "colorSubcolor",
                  "title": "Farbe",
                  "groups": [
                    {
                      "values": [
                        {
                          "sku": "000000003000139078",
                          "imageUrl": "/subcolor-4175-421323.png",
                          "selected": true
                        },
                        {
                          "sku": "000000003000139075",
                          "imageUrl": "/subcolor-4241-421371.png",
                          "selected": false
                        },
                        {
                          "sku": "000000003000139072",
                          "imageUrl": "/subcolor-4314-421473.png",
                          "selected": false
                        },
                        {
                          "sku": "000000003000111897",
                          "imageUrl": "/subcolor-8215-421524.png",
                          "selected": false
                        },
                        {
                          "sku": "000000003000139079",
                          "imageUrl": "/subcolor-4198-421611.png",
                          "selected": false
                        },
                        {
                          "sku": "000000003000139070",
                          "imageUrl": "/subcolor-4504-421705.png",
                          "selected": false
                        }
                      ]
                    }
                  ]
                },
                {
                  "renderingType": "text",
                  "key": "sideLongchair",
                  "title": "Ausrichtung",
                  "groups": [
                    {
                      "values": [
                        {
                          "sku": "000000003000139078",
                          "imageUrl": null,
                          "selected": true
                        },
                        {
                          "sku": "000000003000228307",
                          "imageUrl": null,
                          "selected": false
                        }
                      ]
                    }
                  ]
                }
              ],
              "taxonomyBreadcrumbs": [
                {
                  "name": "Startseite",
                  "url": "/"
                },
                {
                  "name": "Möbel",
                  "url": "moebel-sortiment/"
                },
                {
                  "name": "Sofas & Couches",
                  "url": "sofa-couch/"
                },
                {
                  "name": "Ecksofas",
                  "url": "eckcouches/"
                }
              ]
            },
            {
              "sku": "000000001000016119",
              "parentSku": "000000008000001098",
              "url": "produkt/kuechensofa-tilanda-webstoff-cors-mintgrau",
              "name": "Küchensofa TILANDA",
              "trackingId": null,
              "images": [
                {
                  "path": "https://cdn1.home24.net/images/media/catalog/product/original/png/k/u/kuechensofa-tilanda-webstoff-eiche-massiv-mintgrau-5061644.webp"
                }
              ],
              "labels": [
                {
                  "type": "campaign",
                  "value": "EXTRA -12 %"
                }
              ],
              "delivery": {
                "flags": {
                  "fastDelivery": true
                },
                "time": {
                  "numberOfDays": 3
                }
              },
              "prices": {
                "regular": {
                  "value": 32999,
                  "isRrp": false
                },
                "special": null
              },
              "sellable": true,
              "ratings": {
                "count": 266,
                "average": 4.6
              },
              "brand": {
                "name": "Mørteens"
              },
              "breadcrumbs": [
                {
                  "name": "Startseite"
                },
                {
                  "name": "Möbel"
                },
                {
                  "name": "Bänke"
                },
                {
                  "name": "Sitzbänke"
                }
              ],
              "campaigns": [
                {
                  "key": "xhcgspx-flash12de-0823"
                },
                {
                  "key": "h24campaigns"
                },
                {
                  "key": "influencer"
                },
                {
                  "key": "nonspecial-mp-reduced"
                },
                {
                  "key": "nonspecial-nonmp"
                },
                {
                  "key": "meinestadt"
                },
                {
                  "key": "xlgspx-showroom-all"
                },
                {
                  "key": "backuppayback"
                },
                {
                  "key": "h24-butlers-dining"
                },
                {
                  "key": "nonmp"
                },
                {
                  "key": "showroom-berlinSR"
                }
              ],
              "variantName": "Webstoff Cors: Mintgrau",
              "energyClass": null,
              "sponsored": null,
              "variations": [
                {
                  "renderingType": "icon",
                  "key": "fabric",
                  "title": "Material",
                  "groups": [
                    {
                      "values": [
                        {
                          "sku": "000000001000016124",
                          "imageUrl": "/textileCorsCurryyellow.png",
                          "selected": false
                        },
                        {
                          "sku": "000000001000016120",
                          "imageUrl": "/textileCorsDarkgrey.png",
                          "selected": false
                        },
                        {
                          "sku": "000000001000016123",
                          "imageUrl": "/textileCorsGranite.png",
                          "selected": false
                        },
                        {
                          "sku": "000000001000016119",
                          "imageUrl": "/textileCorsMintgrey.png",
                          "selected": true
                        },
                        {
                          "sku": "000000001000016125",
                          "imageUrl": "/textileCorsPetrol.png",
                          "selected": false
                        }
                      ]
                    }
                  ]
                }
              ],
              "taxonomyBreadcrumbs": [
                {
                  "name": "Startseite",
                  "url": "/"
                },
                {
                  "name": "Möbel",
                  "url": "moebel-sortiment/"
                },
                {
                  "name": "Bänke",
                  "url": "baenke/"
                },
                {
                  "name": "Sitzbänke",
                  "url": "alle-sitzbaenke/"
                }
              ]
            },
            {
              "sku": "000000001000315644",
              "parentSku": "000000008000041427",
              "url": "produkt/taschenfederkernmatratze-royal-plus-gel-90-x-200cm-h3",
              "name": "Taschenfederkernmatratze Royal Plus Gel",
              "trackingId": null,
              "images": [
                {
                  "path": "https://cdn1.home24.net/images/media/catalog/product/original/png/-/1/-1000315644-211215-07441400095-IMAGE-P000000001000315644.webp"
                }
              ],
              "labels": [
                {
                  "type": "campaign",
                  "value": "EXTRA -12 %"
                }
              ],
              "delivery": {
                "flags": {
                  "fastDelivery": false
                },
                "time": {
                  "numberOfDays": 7
                }
              },
              "prices": {
                "regular": {
                  "value": 21999,
                  "isRrp": false
                },
                "special": null
              },
              "sellable": true,
              "ratings": {
                "count": 50,
                "average": 4.6
              },
              "brand": {
                "name": "BeCo"
              },
              "breadcrumbs": [
                {
                  "name": "Startseite"
                },
                {
                  "name": "Möbel"
                },
                {
                  "name": "Matratzen"
                },
                {
                  "name": "Federkernmatratzen"
                }
              ],
              "campaigns": [
                {
                  "key": "xhcgspx-flash12de-0823"
                },
                {
                  "key": "h24campaigns"
                },
                {
                  "key": "influencer"
                },
                {
                  "key": "nonspecial-mp-reduced"
                },
                {
                  "key": "nonspecial-nonmp"
                },
                {
                  "key": "meinestadt"
                },
                {
                  "key": "nonmp"
                }
              ],
              "variantName": "90 x 200cm - H3",
              "energyClass": null,
              "sponsored": null,
              "variations": [
                {
                  "renderingType": "text",
                  "key": "bedSize",
                  "title": "Liegefläche",
                  "groups": [
                    {
                      "values": [
                        {
                          "sku": "000000001000315643",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000001000315632",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000001000315644",
                          "imageUrl": "/bedsize-2240-4088484.png",
                          "selected": true
                        },
                        {
                          "sku": "000000001000315640",
                          "imageUrl": "/bedsize-2215-4088488.png",
                          "selected": false
                        },
                        {
                          "sku": "000000001000315641",
                          "imageUrl": "/bedsize-2222-4088492.png",
                          "selected": false
                        },
                        {
                          "sku": "000000001000315637",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000001000315652",
                          "imageUrl": "/bedsize-2229-4088496.png",
                          "selected": false
                        }
                      ]
                    }
                  ]
                },
                {
                  "renderingType": "text",
                  "key": "degreeOfHardness",
                  "title": "Härtegrad",
                  "groups": [
                    {
                      "values": [
                        {
                          "sku": "000000001000315634",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000001000315644",
                          "imageUrl": null,
                          "selected": true
                        }
                      ]
                    }
                  ]
                }
              ],
              "taxonomyBreadcrumbs": [
                {
                  "name": "Startseite",
                  "url": "/"
                },
                {
                  "name": "Möbel",
                  "url": "moebel-sortiment/"
                },
                {
                  "name": "Matratzen",
                  "url": "matratze/"
                },
                {
                  "name": "Federkernmatratzen",
                  "url": "federkernmatratze/"
                }
              ]
            },
            {
              "sku": "000000003000602440",
              "parentSku": "000000008500126055",
              "url": "produkt/bemmi-tv-schrank-dunkelgruen",
              "name": "BEMMI TV-Schrank",
              "trackingId": null,
              "images": [
                {
                  "path": "https://cdn1.home24.net/images/media/catalog/product/original/jpg/b/1/b11f414e6a194874a4c91ae76cc07e7f.webp"
                }
              ],
              "labels": [
                {
                  "type": "new",
                  "value": "Neu"
                }
              ],
              "delivery": {
                "flags": {
                  "fastDelivery": false
                },
                "time": {
                  "numberOfDays": 32
                }
              },
              "prices": {
                "regular": {
                  "value": 10699,
                  "isRrp": false
                },
                "special": null
              },
              "sellable": true,
              "ratings": {
                "count": 0,
                "average": 0
              },
              "brand": {
                "name": "Selsey"
              },
              "breadcrumbs": [
                {
                  "name": "Startseite"
                },
                {
                  "name": "Möbel"
                },
                {
                  "name": "TV-Möbel"
                },
                {
                  "name": "TV-Lowboards"
                }
              ],
              "campaigns": [
                {
                  "key": "MP-special"
                }
              ],
              "variantName": "Dunkelgrün",
              "energyClass": null,
              "sponsored": null,
              "variations": [
                {
                  "renderingType": "icon",
                  "key": "colorSubcolor",
                  "title": "Farbe",
                  "groups": [
                    {
                      "values": [
                        {
                          "sku": "000000003000602440",
                          "imageUrl": "/subcolor-16498-893698.png",
                          "selected": true
                        },
                        {
                          "sku": "000000003000602442",
                          "imageUrl": "/subcolor-38544-3776060.png",
                          "selected": false
                        }
                      ]
                    }
                  ]
                }
              ],
              "taxonomyBreadcrumbs": [
                {
                  "name": "Startseite",
                  "url": "/"
                },
                {
                  "name": "Möbel",
                  "url": "moebel-sortiment/"
                },
                {
                  "name": "TV-Möbel",
                  "url": "fernseh-tv-moebel/"
                },
                {
                  "name": "TV-Lowboards",
                  "url": "tv-lowboards/"
                }
              ]
            },
            {
              "sku": "000000001000076667",
              "parentSku": "000000008000008336",
              "url": "produkt/schwebetuerenschrank-subito-1-spiegeltuer-alpinweiss-breite-181-cm",
              "name": "Schwebetürenschrank Subito",
              "trackingId": null,
              "images": [
                {
                  "path": "https://cdn1.home24.net/images/media/catalog/product/original/png/s/c/schwebetuerenschrank-subito-1-spiegeltuer-alpinweiss-181-cm-2-tuerig-461892.webp"
                }
              ],
              "labels": [
                {
                  "type": "campaign",
                  "value": "SPECIAL"
                }
              ],
              "delivery": {
                "flags": {
                  "fastDelivery": true
                },
                "time": {
                  "numberOfDays": 10
                }
              },
              "prices": {
                "regular": {
                  "value": 37999,
                  "isRrp": false
                },
                "special": null
              },
              "sellable": true,
              "ratings": {
                "count": 221,
                "average": 4.3
              },
              "brand": {
                "name": "rauch Orange"
              },
              "breadcrumbs": [
                {
                  "name": "Startseite"
                },
                {
                  "name": "Möbel"
                },
                {
                  "name": "Schränke"
                },
                {
                  "name": "Kleiderschränke"
                }
              ],
              "campaigns": [
                {
                  "key": "specialdeals"
                },
                {
                  "key": "sustainable"
                },
                {
                  "key": "nonmp"
                }
              ],
              "variantName": "1 Spiegeltür - Alpinweiß - Breite: 181 cm",
              "energyClass": null,
              "sponsored": null,
              "variations": [
                {
                  "renderingType": "text",
                  "key": "width",
                  "title": "Breite",
                  "groups": [
                    {
                      "values": [
                        {
                          "sku": "000000001000076666",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000001000076667",
                          "imageUrl": null,
                          "selected": true
                        }
                      ]
                    }
                  ]
                }
              ],
              "taxonomyBreadcrumbs": [
                {
                  "name": "Startseite",
                  "url": "/"
                },
                {
                  "name": "Möbel",
                  "url": "moebel-sortiment/"
                },
                {
                  "name": "Schränke",
                  "url": "schraenke-stauraum/"
                },
                {
                  "name": "Kleiderschränke",
                  "url": "alle-kleiderschraenke/"
                }
              ]
            },
            {
              "sku": "000000003000592374",
              "parentSku": "000000003000592374-P",
              "url": "produkt/klassischer-sitzsack-hocker-cord",
              "name": "Klassischer Sitzsack + Hocker, Cord",
              "trackingId": null,
              "images": [
                {
                  "path": "https://cdn1.home24.net/images/media/catalog/product/original/jpg/d/b/db63c51a283d44b187a40875f78554f8.webp"
                }
              ],
              "labels": [
                {
                  "type": "new",
                  "value": "Neu"
                }
              ],
              "delivery": {
                "flags": {
                  "fastDelivery": true
                },
                "time": {
                  "numberOfDays": 4
                }
              },
              "prices": {
                "regular": {
                  "value": 11999,
                  "isRrp": false
                },
                "special": null
              },
              "sellable": true,
              "ratings": {
                "count": 0,
                "average": 0
              },
              "brand": {
                "name": "Icon"
              },
              "breadcrumbs": [
                {
                  "name": "Startseite"
                },
                {
                  "name": "Möbel"
                },
                {
                  "name": "Sessel"
                },
                {
                  "name": "Sitzsäcke"
                }
              ],
              "campaigns": [
                {
                  "key": "MP-special"
                }
              ],
              "variantName": "",
              "energyClass": null,
              "sponsored": null,
              "variations": null,
              "taxonomyBreadcrumbs": [
                {
                  "name": "Startseite",
                  "url": "/"
                },
                {
                  "name": "Möbel",
                  "url": "moebel-sortiment/"
                },
                {
                  "name": "Sessel",
                  "url": "alle-sessel/"
                },
                {
                  "name": "Sitzsäcke",
                  "url": "alle-sitzsaecke/"
                }
              ]
            },
            {
              "sku": "000000003000469185",
              "parentSku": "000000008500097836",
              "url": "produkt/lammelo-tv-moebel-weiss-breite-175-cm",
              "name": "LAMMELO - TV-Möbel  Weiß",
              "trackingId": null,
              "images": [
                {
                  "path": "https://cdn1.home24.net/images/media/catalog/product/original/png/d/9/d9a5a86a3ffd4e0d8479931c4d0cd8b6.cropped-0-1120-3501-1498.processed.webp"
                }
              ],
              "labels": [
                {
                  "type": "new",
                  "value": "Neu"
                }
              ],
              "delivery": {
                "flags": {
                  "fastDelivery": true
                },
                "time": {
                  "numberOfDays": 4
                }
              },
              "prices": {
                "regular": {
                  "value": 15399,
                  "isRrp": false
                },
                "special": null
              },
              "sellable": true,
              "ratings": {
                "count": 1,
                "average": 5
              },
              "brand": {
                "name": "Selsey"
              },
              "breadcrumbs": [
                {
                  "name": "Startseite"
                },
                {
                  "name": "Möbel"
                },
                {
                  "name": "TV-Möbel"
                },
                {
                  "name": "TV-Lowboards"
                }
              ],
              "campaigns": [
                {
                  "key": "MP-special"
                }
              ],
              "variantName": "Breite: 175 cm",
              "energyClass": null,
              "sponsored": null,
              "variations": [
                {
                  "renderingType": "text",
                  "key": "width",
                  "title": "Breite",
                  "groups": [
                    {
                      "values": [
                        {
                          "sku": "000000003000469193",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000003000469185",
                          "imageUrl": null,
                          "selected": true
                        }
                      ]
                    }
                  ]
                }
              ],
              "taxonomyBreadcrumbs": [
                {
                  "name": "Startseite",
                  "url": "/"
                },
                {
                  "name": "Möbel",
                  "url": "moebel-sortiment/"
                },
                {
                  "name": "TV-Möbel",
                  "url": "fernseh-tv-moebel/"
                },
                {
                  "name": "TV-Lowboards",
                  "url": "tv-lowboards/"
                }
              ]
            },
            {
              "sku": "000000003000499440",
              "parentSku": "000000003000499440-P",
              "url": "produkt/polsterbett-boxspringbett-phoebe",
              "name": "Polsterbett Boxspringbett Phoebe Ⅰ",
              "trackingId": null,
              "images": [
                {
                  "path": "https://cdn1.home24.net/images/media/catalog/product/original/jpg/2/0/2059e4b17f68436b8983ab018654c74d.webp"
                }
              ],
              "labels": [
                {
                  "type": "new",
                  "value": "Neu"
                }
              ],
              "delivery": {
                "flags": {
                  "fastDelivery": true
                },
                "time": {
                  "numberOfDays": 7
                }
              },
              "prices": {
                "regular": {
                  "value": 37899,
                  "isRrp": false
                },
                "special": null
              },
              "sellable": true,
              "ratings": {
                "count": 0,
                "average": 0
              },
              "brand": {
                "name": "Merax"
              },
              "breadcrumbs": [
                {
                  "name": "Startseite"
                },
                {
                  "name": "Möbel"
                },
                {
                  "name": "Betten"
                },
                {
                  "name": "Bettgestelle"
                }
              ],
              "campaigns": [
                {
                  "key": "MP-special"
                }
              ],
              "variantName": "",
              "energyClass": null,
              "sponsored": null,
              "variations": null,
              "taxonomyBreadcrumbs": [
                {
                  "name": "Startseite",
                  "url": "/"
                },
                {
                  "name": "Möbel",
                  "url": "moebel-sortiment/"
                },
                {
                  "name": "Betten",
                  "url": "alle-betten/"
                },
                {
                  "name": "Bettgestelle",
                  "url": "bettgestelle/"
                }
              ]
            },
            {
              "sku": "000000001000298796",
              "parentSku": "000000001000298796-P",
              "url": "produkt/kleiderschrank-jeffrey-ii",
              "name": "Kleiderschrank Jeffrey II",
              "trackingId": null,
              "images": [
                {
                  "path": "https://cdn1.home24.net/images/media/catalog/product/original/png/-/1/-1000298796-211013-16103500078-IMAGE-P000000001000298796.webp"
                }
              ],
              "labels": [
                {
                  "type": "campaign",
                  "value": "SPECIAL"
                }
              ],
              "delivery": {
                "flags": {
                  "fastDelivery": true
                },
                "time": {
                  "numberOfDays": 3
                }
              },
              "prices": {
                "regular": {
                  "value": 13999,
                  "isRrp": false
                },
                "special": null
              },
              "sellable": true,
              "ratings": {
                "count": 13,
                "average": 3.5
              },
              "brand": {
                "name": "loftscape"
              },
              "breadcrumbs": [
                {
                  "name": "Startseite"
                },
                {
                  "name": "Möbel"
                },
                {
                  "name": "Schränke"
                },
                {
                  "name": "Kleiderschränke"
                }
              ],
              "campaigns": [
                {
                  "key": "specialdeals"
                },
                {
                  "key": "firstflat"
                },
                {
                  "key": "nonmp"
                }
              ],
              "variantName": "",
              "energyClass": null,
              "sponsored": null,
              "variations": null,
              "taxonomyBreadcrumbs": [
                {
                  "name": "Startseite",
                  "url": "/"
                },
                {
                  "name": "Möbel",
                  "url": "moebel-sortiment/"
                },
                {
                  "name": "Schränke",
                  "url": "schraenke-stauraum/"
                },
                {
                  "name": "Kleiderschränke",
                  "url": "alle-kleiderschraenke/"
                }
              ]
            },
            {
              "sku": "000000003000024552",
              "parentSku": "000000008500002575",
              "url": "produkt/schuhschrank-basil-weiss-115-x-79-cm-c6d8",
              "name": "Schuhschrank BASIL",
              "trackingId": null,
              "images": [
                {
                  "path": "https://cdn1.home24.net/images/media/catalog/product/original/jpg/1/2/12f14700da9b48f8ab8b2229b2736af4.webp"
                }
              ],
              "labels": [],
              "delivery": {
                "flags": {
                  "fastDelivery": true
                },
                "time": {
                  "numberOfDays": 5
                }
              },
              "prices": {
                "regular": {
                  "value": 17995,
                  "isRrp": false
                },
                "special": null
              },
              "sellable": true,
              "ratings": {
                "count": 3,
                "average": 3.3
              },
              "brand": {
                "name": "IDIMEX"
              },
              "breadcrumbs": [
                {
                  "name": "Startseite"
                },
                {
                  "name": "Möbel"
                },
                {
                  "name": "Garderoben"
                },
                {
                  "name": "Schuhschränke"
                },
                {
                  "name": "Schuhkipper"
                }
              ],
              "campaigns": [
                {
                  "key": "MP-special"
                }
              ],
              "variantName": "Weiß - 115 x 79 cm",
              "energyClass": null,
              "sponsored": null,
              "variations": [
                {
                  "renderingType": "icon",
                  "key": "colorSubcolor",
                  "title": "Farbe",
                  "groups": [
                    {
                      "values": [
                        {
                          "sku": "000000003000024602",
                          "imageUrl": "/subcolor-4241-421371.png",
                          "selected": false
                        },
                        {
                          "sku": "000000003000024580",
                          "imageUrl": "/subcolor-4314-421473.png",
                          "selected": false
                        },
                        {
                          "sku": "000000003000024552",
                          "imageUrl": "/subcolor-4097-421717.png",
                          "selected": true
                        }
                      ]
                    }
                  ]
                },
                {
                  "renderingType": "text",
                  "key": "_width_height",
                  "title": "Breite x Höhe",
                  "groups": [
                    {
                      "values": [
                        {
                          "sku": "000000003000064039",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000003000024552",
                          "imageUrl": null,
                          "selected": true
                        }
                      ]
                    }
                  ]
                }
              ],
              "taxonomyBreadcrumbs": [
                {
                  "name": "Startseite",
                  "url": "/"
                },
                {
                  "name": "Möbel",
                  "url": "moebel-sortiment/"
                },
                {
                  "name": "Garderoben",
                  "url": "flur-garderoben/"
                },
                {
                  "name": "Schuhschränke",
                  "url": "schuhschraenke/"
                },
                {
                  "name": "Schuhkipper",
                  "url": "schuhkipper/"
                }
              ]
            },
            {
              "sku": "000000003000348767",
              "parentSku": "000000008500065553",
              "url": "produkt/tv-lowboard-telire",
              "name": "TV-Lowboard Telire",
              "trackingId": null,
              "images": [
                {
                  "path": "https://cdn1.home24.net/images/media/catalog/product/original/png/e/4/e4c39a01265e4d28a26ae68e36ef068e.webp"
                }
              ],
              "labels": [],
              "delivery": {
                "flags": {
                  "fastDelivery": true
                },
                "time": {
                  "numberOfDays": 4
                }
              },
              "prices": {
                "regular": {
                  "value": 14399,
                  "isRrp": false
                },
                "special": null
              },
              "sellable": true,
              "ratings": {
                "count": 0,
                "average": 0
              },
              "brand": {
                "name": "Selsey"
              },
              "breadcrumbs": [
                {
                  "name": "Startseite"
                },
                {
                  "name": "Möbel"
                },
                {
                  "name": "TV-Möbel"
                },
                {
                  "name": "TV-Lowboards"
                }
              ],
              "campaigns": [
                {
                  "key": "MP-special"
                }
              ],
              "variantName": "",
              "energyClass": null,
              "sponsored": null,
              "variations": null,
              "taxonomyBreadcrumbs": [
                {
                  "name": "Startseite",
                  "url": "/"
                },
                {
                  "name": "Möbel",
                  "url": "moebel-sortiment/"
                },
                {
                  "name": "TV-Möbel",
                  "url": "fernseh-tv-moebel/"
                },
                {
                  "name": "TV-Lowboards",
                  "url": "tv-lowboards/"
                }
              ]
            },
            {
              "sku": "000000001000008072",
              "parentSku": "000000008000024197",
              "url": "produkt/schuhkipper-glaze-iii-hochglanz-weiss-hoehe-120-cm",
              "name": "Schuhkipper Glaze III",
              "trackingId": null,
              "images": [
                {
                  "path": "https://cdn1.home24.net/images/media/catalog/product/original/png/-/1/-1000008072-180723-17511303-IMAGE-P000000001000008072.webp"
                }
              ],
              "labels": [
                {
                  "type": "campaign",
                  "value": "EXTRA -12 %"
                }
              ],
              "delivery": {
                "flags": {
                  "fastDelivery": true
                },
                "time": {
                  "numberOfDays": 3
                }
              },
              "prices": {
                "regular": {
                  "value": 12999,
                  "isRrp": false
                },
                "special": null
              },
              "sellable": true,
              "ratings": {
                "count": 200,
                "average": 4.6
              },
              "brand": {
                "name": "Fredriks"
              },
              "breadcrumbs": [
                {
                  "name": "Startseite"
                },
                {
                  "name": "Möbel"
                },
                {
                  "name": "Garderoben"
                },
                {
                  "name": "Schuhschränke"
                },
                {
                  "name": "Schuhkipper"
                }
              ],
              "campaigns": [
                {
                  "key": "xhcgspx-flash12de-0823"
                },
                {
                  "key": "h24campaigns"
                },
                {
                  "key": "influencer"
                },
                {
                  "key": "nonspecial-mp-reduced"
                },
                {
                  "key": "nonspecial-nonmp"
                },
                {
                  "key": "firstflat"
                },
                {
                  "key": "meinestadt"
                },
                {
                  "key": "backuppayback"
                },
                {
                  "key": "nonmp"
                }
              ],
              "variantName": "Hochglanz Weiß - Höhe: 120 cm",
              "energyClass": null,
              "sponsored": null,
              "variations": [
                {
                  "renderingType": "icon",
                  "key": "colorSubcolor",
                  "title": "Farbe",
                  "groups": [
                    {
                      "values": [
                        {
                          "sku": "000000001000370257",
                          "imageUrl": "/subcolor-13125-620966.png",
                          "selected": false
                        },
                        {
                          "sku": "000000001000008072",
                          "imageUrl": "/subcolor-4098-482619.png",
                          "selected": true
                        }
                      ]
                    }
                  ]
                },
                {
                  "renderingType": "text",
                  "key": "height",
                  "title": "Höhe",
                  "groups": [
                    {
                      "values": [
                        {
                          "sku": "000000001000008072",
                          "imageUrl": null,
                          "selected": true
                        },
                        {
                          "sku": "000000001000008071",
                          "imageUrl": null,
                          "selected": false
                        }
                      ]
                    }
                  ]
                }
              ],
              "taxonomyBreadcrumbs": [
                {
                  "name": "Startseite",
                  "url": "/"
                },
                {
                  "name": "Möbel",
                  "url": "moebel-sortiment/"
                },
                {
                  "name": "Garderoben",
                  "url": "flur-garderoben/"
                },
                {
                  "name": "Schuhschränke",
                  "url": "schuhschraenke/"
                },
                {
                  "name": "Schuhkipper",
                  "url": "schuhkipper/"
                }
              ]
            },
            {
              "sku": "000000001000076668",
              "parentSku": "000000008000008337",
              "url": "produkt/schwebetuerenschrank-subito-2-spiegeltueren-alpinweiss-breite-136-cm",
              "name": "Schwebetürenschrank Subito",
              "trackingId": null,
              "images": [
                {
                  "path": "https://cdn1.home24.net/images/media/catalog/product/original/png/s/c/schwebetuerenschrank-subito-2-spiegeltueren-alpinweiss-136-cm-2-tuerig-461822.webp"
                }
              ],
              "labels": [
                {
                  "type": "campaign",
                  "value": "EXTRA -12 %"
                }
              ],
              "delivery": {
                "flags": {
                  "fastDelivery": true
                },
                "time": {
                  "numberOfDays": 10
                }
              },
              "prices": {
                "regular": {
                  "value": 37999,
                  "isRrp": false
                },
                "special": null
              },
              "sellable": true,
              "ratings": {
                "count": 68,
                "average": 4.3
              },
              "brand": {
                "name": "rauch Orange"
              },
              "breadcrumbs": [
                {
                  "name": "Startseite"
                },
                {
                  "name": "Möbel"
                },
                {
                  "name": "Schränke"
                },
                {
                  "name": "Kleiderschränke"
                }
              ],
              "campaigns": [
                {
                  "key": "xhcgspx-flash12de-0823"
                },
                {
                  "key": "specialdeals"
                },
                {
                  "key": "h24campaigns"
                },
                {
                  "key": "sustainable"
                },
                {
                  "key": "nonmp"
                }
              ],
              "variantName": "2 Spiegeltüren - Alpinweiß - Breite: 136 cm",
              "energyClass": null,
              "sponsored": null,
              "variations": [
                {
                  "renderingType": "text",
                  "key": "width",
                  "title": "Breite",
                  "groups": [
                    {
                      "values": [
                        {
                          "sku": "000000001000076668",
                          "imageUrl": null,
                          "selected": true
                        },
                        {
                          "sku": "000000001000076669",
                          "imageUrl": null,
                          "selected": false
                        }
                      ]
                    }
                  ]
                }
              ],
              "taxonomyBreadcrumbs": [
                {
                  "name": "Startseite",
                  "url": "/"
                },
                {
                  "name": "Möbel",
                  "url": "moebel-sortiment/"
                },
                {
                  "name": "Schränke",
                  "url": "schraenke-stauraum/"
                },
                {
                  "name": "Kleiderschränke",
                  "url": "alle-kleiderschraenke/"
                }
              ]
            },
            {
              "sku": "000000001000188579",
              "parentSku": "000000008000025872",
              "url": "produkt/lattenrost-rubin-i-buche-natur-anthrazit-90-x-200cm",
              "name": "Lattenrost Rubin I",
              "trackingId": null,
              "images": [
                {
                  "path": "https://cdn1.home24.net/images/media/catalog/product/original/png/-/1/-1000188579-191001-17214500001-IMAGE-P000000001000188579.webp"
                }
              ],
              "labels": [],
              "delivery": {
                "flags": {
                  "fastDelivery": false
                },
                "time": {
                  "numberOfDays": 18
                }
              },
              "prices": {
                "regular": {
                  "value": 7199,
                  "isRrp": false
                },
                "special": null
              },
              "sellable": true,
              "ratings": {
                "count": 38,
                "average": 3.9
              },
              "brand": {
                "name": "mooved"
              },
              "breadcrumbs": [
                {
                  "name": "Startseite"
                },
                {
                  "name": "Möbel"
                },
                {
                  "name": "Lattenroste"
                },
                {
                  "name": "Federholzlattenroste"
                }
              ],
              "campaigns": [
                {
                  "key": "influencer"
                },
                {
                  "key": "nonspecial-mp-reduced"
                },
                {
                  "key": "nonspecial-nonmp"
                },
                {
                  "key": "xdgspx-googleshopping"
                },
                {
                  "key": "meinestadt"
                },
                {
                  "key": "nonmp"
                }
              ],
              "variantName": "Buche - Natur / Anthrazit - 90 x 200cm",
              "energyClass": null,
              "sponsored": null,
              "variations": [
                {
                  "renderingType": "text",
                  "key": "bedSize",
                  "title": "Liegefläche",
                  "groups": [
                    {
                      "values": [
                        {
                          "sku": "000000001000188586",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000001000188579",
                          "imageUrl": "/bedsize-2240-4088484.png",
                          "selected": true
                        }
                      ]
                    }
                  ]
                }
              ],
              "taxonomyBreadcrumbs": [
                {
                  "name": "Startseite",
                  "url": "/"
                },
                {
                  "name": "Möbel",
                  "url": "moebel-sortiment/"
                },
                {
                  "name": "Lattenroste",
                  "url": "lattenrost/"
                },
                {
                  "name": "Federholzlattenroste",
                  "url": "federholzlattenroste/"
                }
              ]
            },
            {
              "sku": "000000001000383150",
              "parentSku": "000000008000049531",
              "url": "produkt/schuhkommode-ameca-110-cm-graphit",
              "name": "Schuhkommode Ameca 110 cm",
              "trackingId": null,
              "images": [
                {
                  "path": "https://cdn1.home24.net/images/media/catalog/product/original/png/-/1/-1000383150-230418-010-IMAGE-P000000001000383150.webp"
                }
              ],
              "labels": [
                {
                  "type": "campaign",
                  "value": "EXTRA -12 %"
                },
                {
                  "type": "discount",
                  "value": "-46%"
                }
              ],
              "delivery": {
                "flags": {
                  "fastDelivery": true
                },
                "time": {
                  "numberOfDays": 4
                }
              },
              "prices": {
                "regular": {
                  "value": 46900,
                  "isRrp": true
                },
                "special": {
                  "value": 24999,
                  "discount": 21901
                }
              },
              "sellable": true,
              "ratings": {
                "count": 0,
                "average": 0
              },
              "brand": {
                "name": "Germania"
              },
              "breadcrumbs": [
                {
                  "name": "Startseite"
                },
                {
                  "name": "Möbel"
                },
                {
                  "name": "Garderoben"
                },
                {
                  "name": "Schuhschränke"
                },
                {
                  "name": "Schuhkommoden"
                }
              ],
              "campaigns": [
                {
                  "key": "xhcgspx-flash12de-0823"
                },
                {
                  "key": "sale40"
                },
                {
                  "key": "h24campaigns"
                },
                {
                  "key": "influencer"
                },
                {
                  "key": "nonspecial-nonmp"
                },
                {
                  "key": "sale"
                },
                {
                  "key": "meinestadt"
                },
                {
                  "key": "nonmp"
                }
              ],
              "variantName": "Graphit",
              "energyClass": null,
              "sponsored": null,
              "variations": [
                {
                  "renderingType": "icon",
                  "key": "colorSubcolor",
                  "title": "Farbe",
                  "groups": [
                    {
                      "values": [
                        {
                          "sku": "000000001000383150",
                          "imageUrl": "/subcolor-4219-421348.png",
                          "selected": true
                        },
                        {
                          "sku": "000000001000383154",
                          "imageUrl": "/greygreen.png",
                          "selected": false
                        }
                      ]
                    }
                  ]
                }
              ],
              "taxonomyBreadcrumbs": [
                {
                  "name": "Startseite",
                  "url": "/"
                },
                {
                  "name": "Möbel",
                  "url": "moebel-sortiment/"
                },
                {
                  "name": "Garderoben",
                  "url": "flur-garderoben/"
                },
                {
                  "name": "Schuhschränke",
                  "url": "schuhschraenke/"
                },
                {
                  "name": "Schuhkommoden",
                  "url": "schuhkommode/"
                }
              ]
            },
            {
              "sku": "000000001000327457",
              "parentSku": "000000008000042432",
              "url": "produkt/wohnwand-tyfta-i-4-teilig-eiche-grandson-dekor",
              "name": "Wohnwand Tyfta I (4-teilig)",
              "trackingId": null,
              "images": [
                {
                  "path": "https://cdn1.home24.net/images/media/catalog/product/original/png/-/1/-1000327457-220125-13413400011-MOOD-IMAGE-P000000001000327457-mood.webp"
                }
              ],
              "labels": [],
              "delivery": {
                "flags": {
                  "fastDelivery": true
                },
                "time": {
                  "numberOfDays": 10
                }
              },
              "prices": {
                "regular": {
                  "value": 87999,
                  "isRrp": false
                },
                "special": null
              },
              "sellable": true,
              "ratings": {
                "count": 15,
                "average": 4.6
              },
              "brand": {
                "name": "loftscape"
              },
              "breadcrumbs": [
                {
                  "name": "Startseite"
                },
                {
                  "name": "Möbel"
                },
                {
                  "name": "Wohnwände"
                }
              ],
              "campaigns": [
                {
                  "key": "influencer"
                },
                {
                  "key": "nonspecial-mp-reduced"
                },
                {
                  "key": "nonspecial-nonmp"
                },
                {
                  "key": "xdgspx-googleshopping"
                },
                {
                  "key": "meinestadt"
                },
                {
                  "key": "nonmp"
                }
              ],
              "variantName": "Eiche Grandson Dekor",
              "energyClass": null,
              "sponsored": null,
              "variations": [
                {
                  "renderingType": "icon",
                  "key": "colorSubcolor",
                  "title": "Farbe",
                  "groups": [
                    {
                      "values": [
                        {
                          "sku": "000000001000327457",
                          "imageUrl": "/oakGrandsonDecor.png",
                          "selected": true
                        },
                        {
                          "sku": "000000001000327452",
                          "imageUrl": "/subcolor-4098-482619.png",
                          "selected": false
                        }
                      ]
                    }
                  ]
                }
              ],
              "taxonomyBreadcrumbs": [
                {
                  "name": "Startseite",
                  "url": "/"
                },
                {
                  "name": "Möbel",
                  "url": "moebel-sortiment/"
                },
                {
                  "name": "Wohnwände",
                  "url": "wohnwand/"
                }
              ]
            },
            {
              "sku": "000000001000329147",
              "parentSku": "000000001000329147-P",
              "url": "produkt/schuhkommode-miltach-ii-hochglanz-weiss-matt-weiss",
              "name": "Schuhkommode Miltach II",
              "trackingId": null,
              "images": [
                {
                  "path": "https://cdn1.home24.net/images/media/catalog/product/original/png/-/1/-1000329147-220127-17221400010-IMAGE-P000000001000329147.webp"
                }
              ],
              "labels": [],
              "delivery": {
                "flags": {
                  "fastDelivery": false
                },
                "time": {
                  "numberOfDays": 6
                }
              },
              "prices": {
                "regular": {
                  "value": 23499,
                  "isRrp": false
                },
                "special": null
              },
              "sellable": true,
              "ratings": {
                "count": 4,
                "average": 4
              },
              "brand": {
                "name": "loftscape"
              },
              "breadcrumbs": [
                {
                  "name": "Startseite"
                },
                {
                  "name": "Möbel"
                },
                {
                  "name": "Garderoben"
                },
                {
                  "name": "Schuhschränke"
                },
                {
                  "name": "Schuhkommoden"
                }
              ],
              "campaigns": [
                {
                  "key": "influencer"
                },
                {
                  "key": "nonspecial-mp-reduced"
                },
                {
                  "key": "nonspecial-nonmp"
                },
                {
                  "key": "xdgspx-googleshopping"
                },
                {
                  "key": "meinestadt"
                },
                {
                  "key": "nonmp"
                }
              ],
              "variantName": "Hochglanz Weiß / Matt Weiß",
              "energyClass": null,
              "sponsored": null,
              "variations": null,
              "taxonomyBreadcrumbs": [
                {
                  "name": "Startseite",
                  "url": "/"
                },
                {
                  "name": "Möbel",
                  "url": "moebel-sortiment/"
                },
                {
                  "name": "Garderoben",
                  "url": "flur-garderoben/"
                },
                {
                  "name": "Schuhschränke",
                  "url": "schuhschraenke/"
                },
                {
                  "name": "Schuhkommoden",
                  "url": "schuhkommode/"
                }
              ]
            },
            {
              "sku": "000000001000022573",
              "parentSku": "000000008000001979",
              "url": "produkt/lattenrost-medistar-nv-buche-hellbraun-90-x-200cm",
              "name": "Lattenrost MEDISTAR NV",
              "trackingId": null,
              "images": [
                {
                  "path": "https://cdn1.home24.net/images/media/catalog/product/original/png/l/a/lattenrost-medistar-4431992.webp"
                }
              ],
              "labels": [
                {
                  "type": "campaign",
                  "value": "EXTRA -12 %"
                }
              ],
              "delivery": {
                "flags": {
                  "fastDelivery": true
                },
                "time": {
                  "numberOfDays": 10
                }
              },
              "prices": {
                "regular": {
                  "value": 10499,
                  "isRrp": false
                },
                "special": null
              },
              "sellable": true,
              "ratings": {
                "count": 367,
                "average": 4.5
              },
              "brand": {
                "name": "BeCo"
              },
              "breadcrumbs": [
                {
                  "name": "Startseite"
                },
                {
                  "name": "Möbel"
                },
                {
                  "name": "Lattenroste"
                },
                {
                  "name": "Federholzlattenroste"
                }
              ],
              "campaigns": [
                {
                  "key": "xhcgspx-flash12de-0823"
                },
                {
                  "key": "specialdeals"
                },
                {
                  "key": "h24campaigns"
                },
                {
                  "key": "xlgspx-showroom-all"
                },
                {
                  "key": "nonmp"
                },
                {
                  "key": "showroom-dusseldorfSR"
                },
                {
                  "key": "showroom-stuttgartSR"
                }
              ],
              "variantName": "Buche / Hellbraun - 90 x 200cm",
              "energyClass": null,
              "sponsored": null,
              "variations": [
                {
                  "renderingType": "text",
                  "key": "bedSize",
                  "title": "Liegefläche",
                  "groups": [
                    {
                      "values": [
                        {
                          "sku": "000000001000022576",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000001000022577",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000001000022578",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000001000022579",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000001000022580",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000001000022572",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000001000022573",
                          "imageUrl": "/bedsize-2240-4088484.png",
                          "selected": true
                        },
                        {
                          "sku": "000000001000022581",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000001000022582",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000001000022574",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000001000022575",
                          "imageUrl": "/bedsize-2215-4088488.png",
                          "selected": false
                        },
                        {
                          "sku": "000000001000022583",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000001000022584",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000001000022585",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000001000022586",
                          "imageUrl": "/bedsize-2222-4088492.png",
                          "selected": false
                        },
                        {
                          "sku": "000000001000022588",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000001000022587",
                          "imageUrl": null,
                          "selected": false
                        }
                      ]
                    }
                  ]
                }
              ],
              "taxonomyBreadcrumbs": [
                {
                  "name": "Startseite",
                  "url": "/"
                },
                {
                  "name": "Möbel",
                  "url": "moebel-sortiment/"
                },
                {
                  "name": "Lattenroste",
                  "url": "lattenrost/"
                },
                {
                  "name": "Federholzlattenroste",
                  "url": "federholzlattenroste/"
                }
              ]
            },
            {
              "sku": "000000003000068909",
              "parentSku": "000000008500013263",
              "url": "produkt/matratze-120x200-kaltschaummatratze-h2-120-x-200-cm",
              "name": "Matratze 120x200 Kaltschaummatratze H2",
              "trackingId": null,
              "images": [
                {
                  "path": "https://cdn1.home24.net/images/media/catalog/product/original/png/4/1/41ed30a8e3744a9f912af8dae914e910.cropped-0-274-1461-1027.processed.webp"
                }
              ],
              "labels": [
                {
                  "type": "discount",
                  "value": "-50%"
                }
              ],
              "delivery": {
                "flags": {
                  "fastDelivery": true
                },
                "time": {
                  "numberOfDays": 5
                }
              },
              "prices": {
                "regular": {
                  "value": 17990,
                  "isRrp": false
                },
                "special": {
                  "value": 8990,
                  "discount": 9000
                }
              },
              "sellable": true,
              "ratings": {
                "count": 0,
                "average": 0
              },
              "brand": {
                "name": "Mister Sandman"
              },
              "breadcrumbs": [
                {
                  "name": "Startseite"
                },
                {
                  "name": "Möbel"
                },
                {
                  "name": "Matratzen"
                },
                {
                  "name": "Kaltschaummatratzen"
                }
              ],
              "campaigns": [
                {
                  "key": "sale40"
                },
                {
                  "key": "MP-sale"
                },
                {
                  "key": "MP-special"
                }
              ],
              "variantName": "120 x 200 cm",
              "energyClass": null,
              "sponsored": null,
              "variations": [
                {
                  "renderingType": "text",
                  "key": "_width_depth",
                  "title": "Breite x Tiefe",
                  "groups": [
                    {
                      "values": [
                        {
                          "sku": "000000003000068935",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000003000068898",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000003000068965",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000003000068922",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000003000068897",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000003000068893",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000003000068909",
                          "imageUrl": null,
                          "selected": true
                        },
                        {
                          "sku": "000000003000068934",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000003000068903",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000003000068919",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000003000068948",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000003000068946",
                          "imageUrl": null,
                          "selected": false
                        }
                      ]
                    }
                  ]
                }
              ],
              "taxonomyBreadcrumbs": [
                {
                  "name": "Startseite",
                  "url": "/"
                },
                {
                  "name": "Möbel",
                  "url": "moebel-sortiment/"
                },
                {
                  "name": "Matratzen",
                  "url": "matratze/"
                },
                {
                  "name": "Kaltschaummatratzen",
                  "url": "kaltschaummatratze/"
                }
              ]
            },
            {
              "sku": "000000001000355864",
              "parentSku": "000000001000355864-P",
              "url": "produkt/essgruppe-pierry-ii-7-teilig-eiche-massiv-aluminium-samt-stahl-raucheiche-schwarz-taupe-schwarz",
              "name": "Essgruppe Pierry II (7-teilig)",
              "trackingId": null,
              "images": [
                {
                  "path": "https://cdn1.home24.net/images/media/catalog/product/original/png/-/1/-1000355864-220617-011-MOOD-IMAGE-P000000001000355864-mood.webp"
                }
              ],
              "labels": [
                {
                  "type": "campaign",
                  "value": "EXTRA -12 %"
                },
                {
                  "type": "discount",
                  "value": "-12%"
                }
              ],
              "delivery": {
                "flags": {
                  "fastDelivery": false
                },
                "time": {
                  "numberOfDays": 21
                }
              },
              "prices": {
                "regular": {
                  "value": 119999,
                  "isRrp": false
                },
                "special": {
                  "value": 104999,
                  "discount": 15000
                }
              },
              "sellable": true,
              "ratings": {
                "count": 0,
                "average": 0
              },
              "brand": {
                "name": "loftscape"
              },
              "breadcrumbs": [
                {
                  "name": "Startseite"
                },
                {
                  "name": "Möbel"
                },
                {
                  "name": "Möbel-Sets"
                },
                {
                  "name": "Essgruppen"
                }
              ],
              "campaigns": [
                {
                  "key": "xhcgspx-flash12de-0823"
                },
                {
                  "key": "specialdeals"
                },
                {
                  "key": "sale10"
                },
                {
                  "key": "h24campaigns"
                },
                {
                  "key": "sale"
                },
                {
                  "key": "nonmp"
                }
              ],
              "variantName": "Eiche massiv & Aluminium / Samt & Stahl - Raucheiche & Schwarz / Taupe & Schwarz",
              "energyClass": null,
              "sponsored": null,
              "variations": null,
              "taxonomyBreadcrumbs": [
                {
                  "name": "Startseite",
                  "url": "/"
                },
                {
                  "name": "Möbel",
                  "url": "moebel-sortiment/"
                },
                {
                  "name": "Möbel-Sets",
                  "url": "moebel-sets/"
                },
                {
                  "name": "Essgruppen",
                  "url": "essgruppe/"
                }
              ]
            },
            {
              "sku": "000000001000374814",
              "parentSku": "000000008000048534",
              "url": "produkt/armlehnenstuhl-rootsi-2er-set-beige",
              "name": "Armlehnenstuhl Rootsi 2er-Set",
              "trackingId": null,
              "images": [
                {
                  "path": "https://cdn1.home24.net/images/media/catalog/product/original/png/-/1/-1000374814-230208-010-IMAGE-P000000001000374814.webp"
                }
              ],
              "labels": [
                {
                  "type": "campaign",
                  "value": "EXTRA -15 %"
                }
              ],
              "delivery": {
                "flags": {
                  "fastDelivery": false
                },
                "time": {
                  "numberOfDays": 41
                }
              },
              "prices": {
                "regular": {
                  "value": 21999,
                  "isRrp": false
                },
                "special": null
              },
              "sellable": true,
              "ratings": {
                "count": 0,
                "average": 0
              },
              "brand": {
                "name": "Fredriks"
              },
              "breadcrumbs": [
                {
                  "name": "Startseite"
                },
                {
                  "name": "Möbel"
                },
                {
                  "name": "Stühle"
                },
                {
                  "name": "Esszimmerstühle"
                }
              ],
              "campaigns": [
                {
                  "key": "salespecial"
                },
                {
                  "key": "xgspx-chairsdining-0723"
                },
                {
                  "key": "h24campaigns"
                },
                {
                  "key": "influencer"
                },
                {
                  "key": "nonspecial-mp-reduced"
                },
                {
                  "key": "nonspecial-nonmp"
                },
                {
                  "key": "meinestadt"
                },
                {
                  "key": "nonmp"
                }
              ],
              "variantName": "Beige",
              "energyClass": null,
              "sponsored": null,
              "variations": [
                {
                  "renderingType": "icon",
                  "key": "colorSubcolor",
                  "title": "Farbe",
                  "groups": [
                    {
                      "values": [
                        {
                          "sku": "000000001000374814",
                          "imageUrl": "/subcolor-4175-421323.png",
                          "selected": true
                        },
                        {
                          "sku": "000000001000374827",
                          "imageUrl": "/subcolor-11032-491734.png",
                          "selected": false
                        }
                      ]
                    }
                  ]
                }
              ],
              "taxonomyBreadcrumbs": [
                {
                  "name": "Startseite",
                  "url": "/"
                },
                {
                  "name": "Möbel",
                  "url": "moebel-sortiment/"
                },
                {
                  "name": "Stühle",
                  "url": "alle-stuehle/"
                },
                {
                  "name": "Esszimmerstühle",
                  "url": "esszimmerstuhl/"
                }
              ]
            },
            {
              "sku": "000000001000082051",
              "parentSku": "000000008000008676",
              "url": "produkt/drehtuerenschrank-kiydoo-landhaus-i-alpinweiss-226-x-210-cm",
              "name": "Drehtürenschrank KiYDOO Landhaus I",
              "trackingId": null,
              "images": [
                {
                  "path": "https://cdn1.home24.net/images/media/catalog/product/original/png/-/1/-1000082051-180605-16125501-IMAGE-P000000001000082051.webp"
                }
              ],
              "labels": [
                {
                  "type": "campaign",
                  "value": "EXTRA -12 %"
                }
              ],
              "delivery": {
                "flags": {
                  "fastDelivery": true
                },
                "time": {
                  "numberOfDays": 10
                }
              },
              "prices": {
                "regular": {
                  "value": 52999,
                  "isRrp": false
                },
                "special": null
              },
              "sellable": true,
              "ratings": {
                "count": 62,
                "average": 4.2
              },
              "brand": {
                "name": "KiYDOO"
              },
              "breadcrumbs": [
                {
                  "name": "Startseite"
                },
                {
                  "name": "Möbel"
                },
                {
                  "name": "Schränke"
                },
                {
                  "name": "Kleiderschränke"
                }
              ],
              "campaigns": [
                {
                  "key": "xhcgspx-flash12de-0823"
                },
                {
                  "key": "h24campaigns"
                },
                {
                  "key": "influencer"
                },
                {
                  "key": "nonspecial-mp-reduced"
                },
                {
                  "key": "nonspecial-nonmp"
                },
                {
                  "key": "24series"
                },
                {
                  "key": "meinestadt"
                },
                {
                  "key": "sustainable"
                },
                {
                  "key": "butlers"
                },
                {
                  "key": "nonmp"
                }
              ],
              "variantName": "Alpinweiß - 226 x 210 cm",
              "energyClass": null,
              "sponsored": null,
              "variations": [
                {
                  "renderingType": "text",
                  "key": "_width_height",
                  "title": "Breite x Höhe",
                  "groups": [
                    {
                      "values": [
                        {
                          "sku": "000000001000082042",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000001000082043",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000001000082044",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000001000082045",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000001000082046",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000001000082047",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000001000082048",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000001000082049",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000001000082050",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000001000082051",
                          "imageUrl": null,
                          "selected": true
                        }
                      ]
                    }
                  ]
                }
              ],
              "taxonomyBreadcrumbs": [
                {
                  "name": "Startseite",
                  "url": "/"
                },
                {
                  "name": "Möbel",
                  "url": "moebel-sortiment/"
                },
                {
                  "name": "Schränke",
                  "url": "schraenke-stauraum/"
                },
                {
                  "name": "Kleiderschränke",
                  "url": "alle-kleiderschraenke/"
                }
              ]
            },
            {
              "sku": "000000001000174655",
              "parentSku": "000000008000023660",
              "url": "produkt/schlafsessel-elands-buche-natur-webstoff-nims-cappuccino",
              "name": "Schlafsessel Elands Buche Natur",
              "trackingId": null,
              "images": [
                {
                  "path": "https://cdn1.home24.net/images/media/catalog/product/original/png/-/1/-1000174655-190912-12582600130-IMAGE-P000000001000174655.webp"
                }
              ],
              "labels": [
                {
                  "type": "campaign",
                  "value": "HIT"
                }
              ],
              "delivery": {
                "flags": {
                  "fastDelivery": true
                },
                "time": {
                  "numberOfDays": 3
                }
              },
              "prices": {
                "regular": {
                  "value": 24999,
                  "isRrp": false
                },
                "special": null
              },
              "sellable": true,
              "ratings": {
                "count": 229,
                "average": 4.6
              },
              "brand": {
                "name": "Mørteens"
              },
              "breadcrumbs": [
                {
                  "name": "Startseite"
                },
                {
                  "name": "Möbel"
                },
                {
                  "name": "Sessel"
                },
                {
                  "name": "Einzelsessel"
                }
              ],
              "campaigns": [
                {
                  "key": "influencer"
                },
                {
                  "key": "nonspecial-mp-reduced"
                },
                {
                  "key": "nonspecial-nonmp"
                },
                {
                  "key": "xdgspx-googleshopping"
                },
                {
                  "key": "meinestadt"
                },
                {
                  "key": "backuppayback"
                },
                {
                  "key": "nonmp"
                }
              ],
              "variantName": "Webstoff Nims: Cappuccino",
              "energyClass": null,
              "sponsored": null,
              "variations": [
                {
                  "renderingType": "icon",
                  "key": "fabric",
                  "title": "Material",
                  "groups": [
                    {
                      "values": [
                        {
                          "sku": "000000001000372019",
                          "imageUrl": "/textileEldridBeige.png",
                          "selected": false
                        },
                        {
                          "sku": "000000001000372033",
                          "imageUrl": "/textileEldridGrey.png",
                          "selected": false
                        },
                        {
                          "sku": "000000001000372034",
                          "imageUrl": "/textileEldridGreen.png",
                          "selected": false
                        }
                      ]
                    },
                    {
                      "values": [
                        {
                          "sku": "000000001000260255",
                          "imageUrl": "/textileEhmiCreme.png",
                          "selected": false
                        },
                        {
                          "sku": "000000001000376860",
                          "imageUrl": "/textileEhmiLightgrey.png",
                          "selected": false
                        },
                        {
                          "sku": "000000001000376858",
                          "imageUrl": "/textileEhmiCornyellow.png",
                          "selected": false
                        },
                        {
                          "sku": "000000001000376859",
                          "imageUrl": "/textileEhmiSlateblue.png",
                          "selected": false
                        }
                      ]
                    },
                    {
                      "values": [
                        {
                          "sku": "000000001000260275",
                          "imageUrl": "/textileCriadaCreme.png",
                          "selected": false
                        },
                        {
                          "sku": "000000001000376851",
                          "imageUrl": "/textileCriadaDarkblue.png",
                          "selected": false
                        },
                        {
                          "sku": "000000001000260276",
                          "imageUrl": "/textileCriadaDarkgreen.png",
                          "selected": false
                        },
                        {
                          "sku": "000000001000260274",
                          "imageUrl": "/textileCriadaGrey.png",
                          "selected": false
                        }
                      ]
                    },
                    {
                      "values": [
                        {
                          "sku": "000000001000372035",
                          "imageUrl": "/textileElaniGrey.png",
                          "selected": false
                        },
                        {
                          "sku": "000000001000372020",
                          "imageUrl": "/textileElaniWhite.png",
                          "selected": false
                        }
                      ]
                    },
                    {
                      "values": [
                        {
                          "sku": "000000001000174655",
                          "imageUrl": "/textileNimsCappuccino.png",
                          "selected": true
                        },
                        {
                          "sku": "000000001000174654",
                          "imageUrl": "/textileNimsDarkgrey.png",
                          "selected": false
                        },
                        {
                          "sku": "000000001000376850",
                          "imageUrl": "/textileNimsDenimblue.png",
                          "selected": false
                        }
                      ]
                    }
                  ]
                }
              ],
              "taxonomyBreadcrumbs": [
                {
                  "name": "Startseite",
                  "url": "/"
                },
                {
                  "name": "Möbel",
                  "url": "moebel-sortiment/"
                },
                {
                  "name": "Sessel",
                  "url": "alle-sessel/"
                },
                {
                  "name": "Einzelsessel",
                  "url": "einzelsessel/"
                }
              ]
            },
            {
              "sku": "000000001000179077",
              "parentSku": "000000008000024325",
              "url": "produkt/boxspringbett-matara-webstoff-180-x-200cm",
              "name": "Boxspringbett Matara",
              "trackingId": null,
              "images": [
                {
                  "path": "https://cdn1.home24.net/images/media/catalog/product/original/png/-/1/-1000179077-210208-15202500005-IMAGE-P000000001000179077.webp"
                }
              ],
              "labels": [
                {
                  "type": "campaign",
                  "value": "SPECIAL"
                },
                {
                  "type": "discount",
                  "value": "-28%"
                }
              ],
              "delivery": {
                "flags": {
                  "fastDelivery": false
                },
                "time": {
                  "numberOfDays": 44
                }
              },
              "prices": {
                "regular": {
                  "value": 79900,
                  "isRrp": true
                },
                "special": {
                  "value": 56999,
                  "discount": 22901
                }
              },
              "sellable": true,
              "ratings": {
                "count": 123,
                "average": 4.5
              },
              "brand": {
                "name": "Norrwood"
              },
              "breadcrumbs": [
                {
                  "name": "Startseite"
                },
                {
                  "name": "Möbel"
                },
                {
                  "name": "Betten"
                },
                {
                  "name": "Boxspringbetten"
                }
              ],
              "campaigns": [
                {
                  "key": "specialdeals"
                },
                {
                  "key": "sale20"
                },
                {
                  "key": "sale"
                },
                {
                  "key": "bedroombedding"
                },
                {
                  "key": "nonmp"
                }
              ],
              "variantName": "Webstoff - 180 x 200cm",
              "energyClass": null,
              "sponsored": null,
              "variations": [
                {
                  "renderingType": "text",
                  "key": "bedSize",
                  "title": "Liegefläche",
                  "groups": [
                    {
                      "values": [
                        {
                          "sku": "000000001000179078",
                          "imageUrl": "/bedsize-2215-4088488.png",
                          "selected": false
                        },
                        {
                          "sku": "000000001000179070",
                          "imageUrl": "/bedsize-2222-4088492.png",
                          "selected": false
                        },
                        {
                          "sku": "000000001000282930",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000001000179077",
                          "imageUrl": "/bedsize-2229-4088496.png",
                          "selected": true
                        }
                      ]
                    }
                  ]
                }
              ],
              "taxonomyBreadcrumbs": [
                {
                  "name": "Startseite",
                  "url": "/"
                },
                {
                  "name": "Möbel",
                  "url": "moebel-sortiment/"
                },
                {
                  "name": "Betten",
                  "url": "alle-betten/"
                },
                {
                  "name": "Boxspringbetten",
                  "url": "boxspringbett/"
                }
              ]
            },
            {
              "sku": "000000001000297118",
              "parentSku": "000000008000039230",
              "url": "produkt/schrankwand-avin-iv-3-teilig-eiche-sonoma-dekor-weiss",
              "name": "Schrankwand Avin IV (3-teilig)",
              "trackingId": null,
              "images": [
                {
                  "path": "https://cdn1.home24.net/images/media/catalog/product/original/png/-/1/-1000297118-211008-13250700072-MOOD-IMAGE-P000000001000297118-mood.webp"
                }
              ],
              "labels": [
                {
                  "type": "campaign",
                  "value": "EXTRA -12 %"
                }
              ],
              "delivery": {
                "flags": {
                  "fastDelivery": false
                },
                "time": {
                  "numberOfDays": 14
                }
              },
              "prices": {
                "regular": {
                  "value": 59999,
                  "isRrp": false
                },
                "special": null
              },
              "sellable": true,
              "ratings": {
                "count": 4,
                "average": 4.7
              },
              "brand": {
                "name": "Furnitive"
              },
              "breadcrumbs": [
                {
                  "name": "Startseite"
                },
                {
                  "name": "Möbel"
                },
                {
                  "name": "Schränke"
                },
                {
                  "name": "Büroschränke"
                },
                {
                  "name": "Aktenschränke"
                }
              ],
              "campaigns": [
                {
                  "key": "xhcgspx-flash12de-0823"
                },
                {
                  "key": "h24campaigns"
                },
                {
                  "key": "influencer"
                },
                {
                  "key": "nonspecial-mp-reduced"
                },
                {
                  "key": "nonspecial-nonmp"
                },
                {
                  "key": "meinestadt"
                },
                {
                  "key": "nonmp"
                }
              ],
              "variantName": "Eiche Sonoma Dekor / Weiß",
              "energyClass": null,
              "sponsored": null,
              "variations": [
                {
                  "renderingType": "icon",
                  "key": "colorSubcolor",
                  "title": "Farbe",
                  "groups": [
                    {
                      "values": [
                        {
                          "sku": "000000001000297124",
                          "imageUrl": "/oakSonomaDecorGlossyAnthracite.png",
                          "selected": false
                        },
                        {
                          "sku": "000000001000297125",
                          "imageUrl": "/subcolor-4476-421672.png",
                          "selected": false
                        },
                        {
                          "sku": "000000001000297118",
                          "imageUrl": "/subcolor-4478-421674.png",
                          "selected": true
                        },
                        {
                          "sku": "000000001000297136",
                          "imageUrl": "/lightgreyGlossyAnthracite.png",
                          "selected": false
                        },
                        {
                          "sku": "000000001000297135",
                          "imageUrl": "/lightgreyGlossyWhite.png",
                          "selected": false
                        },
                        {
                          "sku": "000000001000297137",
                          "imageUrl": "/lightgreyMatWhite.png",
                          "selected": false
                        },
                        {
                          "sku": "000000001000332902",
                          "imageUrl": "/whiteGlossyAnthracite.png",
                          "selected": false
                        },
                        {
                          "sku": "000000001000332890",
                          "imageUrl": "/subcolor-4097-421717.png",
                          "selected": false
                        }
                      ]
                    }
                  ]
                }
              ],
              "taxonomyBreadcrumbs": [
                {
                  "name": "Startseite",
                  "url": "/"
                },
                {
                  "name": "Möbel",
                  "url": "moebel-sortiment/"
                },
                {
                  "name": "Schränke",
                  "url": "schraenke-stauraum/"
                },
                {
                  "name": "Büroschränke",
                  "url": "bueroschraenke/"
                },
                {
                  "name": "Aktenschränke",
                  "url": "aktenschrank/"
                }
              ]
            },
            {
              "sku": "000000001000175647",
              "parentSku": "000000008000001990",
              "url": "produkt/lattenrost-easy-star-verstellbar-90-x-200cm-ed57",
              "name": "Lattenrost Easy Star (verstellbar)",
              "trackingId": null,
              "images": [
                {
                  "path": "https://cdn1.home24.net/images/media/catalog/product/original/jpg/-/1/-1000175647-190605-08052800001-IMAGE-P000000001000175647.webp"
                }
              ],
              "labels": [
                {
                  "type": "campaign",
                  "value": "EXTRA -15 %"
                }
              ],
              "delivery": {
                "flags": {
                  "fastDelivery": true
                },
                "time": {
                  "numberOfDays": 3
                }
              },
              "prices": {
                "regular": {
                  "value": 8299,
                  "isRrp": false
                },
                "special": null
              },
              "sellable": true,
              "ratings": {
                "count": 268,
                "average": 4.1
              },
              "brand": {
                "name": "BeCo"
              },
              "breadcrumbs": [
                {
                  "name": "Startseite"
                },
                {
                  "name": "Möbel"
                },
                {
                  "name": "Lattenroste"
                },
                {
                  "name": "Federholzlattenroste"
                }
              ],
              "campaigns": [
                {
                  "key": "xhcgspx-flash15de-0823"
                },
                {
                  "key": "h24campaigns"
                },
                {
                  "key": "influencer"
                },
                {
                  "key": "nonspecial-mp-reduced"
                },
                {
                  "key": "nonspecial-nonmp"
                },
                {
                  "key": "meinestadt"
                },
                {
                  "key": "backuppayback"
                },
                {
                  "key": "nonmp"
                }
              ],
              "variantName": "90 x 200cm",
              "energyClass": null,
              "sponsored": null,
              "variations": [
                {
                  "renderingType": "text",
                  "key": "bedSize",
                  "title": "Liegefläche",
                  "groups": [
                    {
                      "values": [
                        {
                          "sku": "000000001000175647",
                          "imageUrl": "/bedsize-2240-4088484.png",
                          "selected": true
                        },
                        {
                          "sku": "000000001000175682",
                          "imageUrl": "/bedsize-2222-4088492.png",
                          "selected": false
                        }
                      ]
                    }
                  ]
                }
              ],
              "taxonomyBreadcrumbs": [
                {
                  "name": "Startseite",
                  "url": "/"
                },
                {
                  "name": "Möbel",
                  "url": "moebel-sortiment/"
                },
                {
                  "name": "Lattenroste",
                  "url": "lattenrost/"
                },
                {
                  "name": "Federholzlattenroste",
                  "url": "federholzlattenroste/"
                }
              ]
            },
            {
              "sku": "000000001000367274",
              "parentSku": "000000008000047538",
              "url": "produkt/bett-kalbarri-eiche-dunkel-dekor-gold-180-x-200cm",
              "name": "Bett Kalbarri",
              "trackingId": null,
              "images": [
                {
                  "path": "https://cdn1.home24.net/images/media/catalog/product/original/png/-/1/-1000367274-221005-010-IMAGE-P000000001000367274.webp"
                }
              ],
              "labels": [
                {
                  "type": "campaign",
                  "value": "EXTRA -12 %"
                }
              ],
              "delivery": {
                "flags": {
                  "fastDelivery": true
                },
                "time": {
                  "numberOfDays": 10
                }
              },
              "prices": {
                "regular": {
                  "value": 36999,
                  "isRrp": false
                },
                "special": null
              },
              "sellable": true,
              "ratings": {
                "count": 12,
                "average": 4.2
              },
              "brand": {
                "name": "Red Living"
              },
              "breadcrumbs": [
                {
                  "name": "Startseite"
                },
                {
                  "name": "Möbel"
                },
                {
                  "name": "Betten"
                },
                {
                  "name": "Bettgestelle"
                }
              ],
              "campaigns": [
                {
                  "key": "xhcgspx-flash12de-0823"
                },
                {
                  "key": "h24campaigns"
                },
                {
                  "key": "influencer"
                },
                {
                  "key": "nonspecial-mp-reduced"
                },
                {
                  "key": "nonspecial-nonmp"
                },
                {
                  "key": "bedroombedding"
                },
                {
                  "key": "meinestadt"
                },
                {
                  "key": "nonmp"
                }
              ],
              "variantName": "Eiche Dunkel Dekor / Gold - 180 x 200cm",
              "energyClass": null,
              "sponsored": null,
              "variations": [
                {
                  "renderingType": "icon",
                  "key": "colorSubcolor",
                  "title": "Farbe",
                  "groups": [
                    {
                      "values": [
                        {
                          "sku": "000000001000367266",
                          "imageUrl": "/oakArtisanDecor.png",
                          "selected": false
                        },
                        {
                          "sku": "000000001000367274",
                          "imageUrl": "/oakDarkDecorGold.png",
                          "selected": true
                        },
                        {
                          "sku": "000000001000367279",
                          "imageUrl": "/oakDarkDecorSilverColored.png",
                          "selected": false
                        }
                      ]
                    }
                  ]
                },
                {
                  "renderingType": "text",
                  "key": "bedSize",
                  "title": "Liegefläche",
                  "groups": [
                    {
                      "values": [
                        {
                          "sku": "000000001000367272",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000001000367274",
                          "imageUrl": "/bedsize-2229-4088496.png",
                          "selected": true
                        }
                      ]
                    }
                  ]
                }
              ],
              "taxonomyBreadcrumbs": [
                {
                  "name": "Startseite",
                  "url": "/"
                },
                {
                  "name": "Möbel",
                  "url": "moebel-sortiment/"
                },
                {
                  "name": "Betten",
                  "url": "alle-betten/"
                },
                {
                  "name": "Bettgestelle",
                  "url": "bettgestelle/"
                }
              ]
            },
            {
              "sku": "000000001000080045",
              "parentSku": "000000008000008557",
              "url": "produkt/schwebetuerenschrank-loriga-alpinweiss-glas-weiss-breite-261-cm",
              "name": "Schwebetürenschrank Loriga",
              "trackingId": null,
              "images": [
                {
                  "path": "https://cdn1.home24.net/images/media/catalog/product/original/png/s/c/schwebetuerenschrank-loriga-alpinweiss-261-cm-2-tuerig-2032626.webp"
                }
              ],
              "labels": [
                {
                  "type": "campaign",
                  "value": "EXTRA -12 %"
                }
              ],
              "delivery": {
                "flags": {
                  "fastDelivery": true
                },
                "time": {
                  "numberOfDays": 10
                }
              },
              "prices": {
                "regular": {
                  "value": 49999,
                  "isRrp": false
                },
                "special": null
              },
              "sellable": true,
              "ratings": {
                "count": 137,
                "average": 4.2
              },
              "brand": {
                "name": "rauch Blue"
              },
              "breadcrumbs": [
                {
                  "name": "Startseite"
                },
                {
                  "name": "Möbel"
                },
                {
                  "name": "Schränke"
                },
                {
                  "name": "Kleiderschränke"
                }
              ],
              "campaigns": [
                {
                  "key": "xhcgspx-flash12de-0823"
                },
                {
                  "key": "h24campaigns"
                },
                {
                  "key": "influencer"
                },
                {
                  "key": "nonspecial-mp-reduced"
                },
                {
                  "key": "nonspecial-nonmp"
                },
                {
                  "key": "meinestadt"
                },
                {
                  "key": "sustainable"
                },
                {
                  "key": "nonmp"
                }
              ],
              "variantName": "Alpinweiß / Glas Weiß - Breite: 261 cm",
              "energyClass": null,
              "sponsored": null,
              "variations": [
                {
                  "renderingType": "icon",
                  "key": "colorSubcolor",
                  "title": "Farbe",
                  "groups": [
                    {
                      "values": [
                        {
                          "sku": "000000001000080051",
                          "imageUrl": "/subcolor-16742-948622.png",
                          "selected": false
                        },
                        {
                          "sku": "000000001000080045",
                          "imageUrl": "/subcolor-14398-692822.png",
                          "selected": true
                        },
                        {
                          "sku": "000000001000080036",
                          "imageUrl": "/subcolor-30326-2296846.png",
                          "selected": false
                        },
                        {
                          "sku": "000000001000080039",
                          "imageUrl": "/subcolor-12856-692606.png",
                          "selected": false
                        },
                        {
                          "sku": "000000001000080048",
                          "imageUrl": "/subcolor-28462-2086710.png",
                          "selected": false
                        },
                        {
                          "sku": "000000001000080042",
                          "imageUrl": "/subcolor-30330-2138574.png",
                          "selected": false
                        }
                      ]
                    }
                  ]
                },
                {
                  "renderingType": "text",
                  "key": "width",
                  "title": "Breite",
                  "groups": [
                    {
                      "values": [
                        {
                          "sku": "000000001000080043",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000001000080044",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000001000080045",
                          "imageUrl": null,
                          "selected": true
                        }
                      ]
                    }
                  ]
                }
              ],
              "taxonomyBreadcrumbs": [
                {
                  "name": "Startseite",
                  "url": "/"
                },
                {
                  "name": "Möbel",
                  "url": "moebel-sortiment/"
                },
                {
                  "name": "Schränke",
                  "url": "schraenke-stauraum/"
                },
                {
                  "name": "Kleiderschränke",
                  "url": "alle-kleiderschraenke/"
                }
              ]
            },
            {
              "sku": "000000001000355878",
              "parentSku": "000000008000046020",
              "url": "produkt/essgruppe-maxou-5-teilig-babyrosa",
              "name": "Essgruppe Maxou (5-teilig)",
              "trackingId": null,
              "images": [
                {
                  "path": "https://cdn1.home24.net/images/media/catalog/product/original/png/-/1/-1000355878-220617-011-MOOD-IMAGE-P000000001000355878-mood.webp"
                }
              ],
              "labels": [
                {
                  "type": "campaign",
                  "value": "EXTRA -12 %"
                },
                {
                  "type": "discount",
                  "value": "-8%"
                }
              ],
              "delivery": {
                "flags": {
                  "fastDelivery": false
                },
                "time": {
                  "numberOfDays": 21
                }
              },
              "prices": {
                "regular": {
                  "value": 67999,
                  "isRrp": false
                },
                "special": {
                  "value": 61999,
                  "discount": 6000
                }
              },
              "sellable": true,
              "ratings": {
                "count": 0,
                "average": 0
              },
              "brand": {
                "name": "Red Living"
              },
              "breadcrumbs": [
                {
                  "name": "Startseite"
                },
                {
                  "name": "Möbel"
                },
                {
                  "name": "Möbel-Sets"
                },
                {
                  "name": "Essgruppen"
                }
              ],
              "campaigns": [
                {
                  "key": "xhcgspx-flash12de-0823"
                },
                {
                  "key": "sale10"
                },
                {
                  "key": "h24campaigns"
                },
                {
                  "key": "influencer"
                },
                {
                  "key": "nonspecial-nonmp"
                },
                {
                  "key": "sale"
                },
                {
                  "key": "meinestadt"
                },
                {
                  "key": "nonmp"
                }
              ],
              "variantName": "Babyrosa",
              "energyClass": null,
              "sponsored": null,
              "variations": [
                {
                  "renderingType": "icon",
                  "key": "colorSubcolor",
                  "title": "Farbe",
                  "groups": [
                    {
                      "values": [
                        {
                          "sku": "000000001000355878",
                          "imageUrl": "/babyrose.png",
                          "selected": true
                        },
                        {
                          "sku": "000000001000355270",
                          "imageUrl": "/subcolor-8215-421524.png",
                          "selected": false
                        },
                        {
                          "sku": "000000001000355877",
                          "imageUrl": "/subcolor-4104-421559.png",
                          "selected": false
                        }
                      ]
                    }
                  ]
                }
              ],
              "taxonomyBreadcrumbs": [
                {
                  "name": "Startseite",
                  "url": "/"
                },
                {
                  "name": "Möbel",
                  "url": "moebel-sortiment/"
                },
                {
                  "name": "Möbel-Sets",
                  "url": "moebel-sets/"
                },
                {
                  "name": "Essgruppen",
                  "url": "essgruppe/"
                }
              ]
            },
            {
              "sku": "000000001000121611",
              "parentSku": "000000008000016402",
              "url": "produkt/drehtuerenschrank-hildesheim-alpinweiss-breite-271-cm-ohne-spiegeltuer-en",
              "name": "Drehtürenschrank Hildesheim",
              "trackingId": null,
              "images": [
                {
                  "path": "https://cdn1.home24.net/images/media/catalog/product/original/png/d/r/drehtuerenschrank-hildesheim-alpinweiss-ohne-spiegeltuer-en-4842896.webp"
                }
              ],
              "labels": [
                {
                  "type": "campaign",
                  "value": "SPECIAL"
                }
              ],
              "delivery": {
                "flags": {
                  "fastDelivery": true
                },
                "time": {
                  "numberOfDays": 10
                }
              },
              "prices": {
                "regular": {
                  "value": 66999,
                  "isRrp": false
                },
                "special": null
              },
              "sellable": true,
              "ratings": {
                "count": 139,
                "average": 4.2
              },
              "brand": {
                "name": "rauch Orange"
              },
              "breadcrumbs": [
                {
                  "name": "Startseite"
                },
                {
                  "name": "Möbel"
                },
                {
                  "name": "Schränke"
                },
                {
                  "name": "Kleiderschränke"
                }
              ],
              "campaigns": [
                {
                  "key": "specialdeals"
                },
                {
                  "key": "sustainable"
                },
                {
                  "key": "nonmp"
                }
              ],
              "variantName": "Alpinweiß - Breite: 271 cm - Ohne Spiegeltür/-en",
              "energyClass": null,
              "sponsored": null,
              "variations": [
                {
                  "renderingType": "icon",
                  "key": "colorSubcolor",
                  "title": "Farbe",
                  "groups": [
                    {
                      "values": [
                        {
                          "sku": "000000001000121610",
                          "imageUrl": "/subcolor-13966-726714.png",
                          "selected": false
                        },
                        {
                          "sku": "000000001000121628",
                          "imageUrl": "/subcolor-13890-726638.png",
                          "selected": false
                        },
                        {
                          "sku": "000000001000121611",
                          "imageUrl": "/subcolor-4128-421294.png",
                          "selected": true
                        }
                      ]
                    }
                  ]
                },
                {
                  "renderingType": "text",
                  "key": "width",
                  "title": "Breite",
                  "groups": [
                    {
                      "values": [
                        {
                          "sku": "000000001000121607",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000001000121611",
                          "imageUrl": null,
                          "selected": true
                        }
                      ]
                    }
                  ]
                },
                {
                  "renderingType": "text",
                  "key": "cabinetMirrors",
                  "title": "Anzahl Spiegeltüren",
                  "groups": [
                    {
                      "values": [
                        {
                          "sku": "000000001000121627",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000001000121611",
                          "imageUrl": null,
                          "selected": true
                        }
                      ]
                    }
                  ]
                }
              ],
              "taxonomyBreadcrumbs": [
                {
                  "name": "Startseite",
                  "url": "/"
                },
                {
                  "name": "Möbel",
                  "url": "moebel-sortiment/"
                },
                {
                  "name": "Schränke",
                  "url": "schraenke-stauraum/"
                },
                {
                  "name": "Kleiderschränke",
                  "url": "alle-kleiderschraenke/"
                }
              ]
            },
            {
              "sku": "000000003000545473",
              "parentSku": "000000008500113673",
              "url": "produkt/cord-wohnlandschaft-rouen-hellgrau",
              "name": "Cord Wohnlandschaft Rouen",
              "trackingId": null,
              "images": [
                {
                  "path": "https://cdn1.home24.net/images/media/catalog/product/original/jpg/2/5/257c0d724d864aaa85d1a15049e158fc.webp"
                }
              ],
              "labels": [
                {
                  "type": "new",
                  "value": "Neu"
                }
              ],
              "delivery": {
                "flags": {
                  "fastDelivery": false
                },
                "time": {
                  "numberOfDays": 19
                }
              },
              "prices": {
                "regular": {
                  "value": 131999,
                  "isRrp": false
                },
                "special": null
              },
              "sellable": true,
              "ratings": {
                "count": 0,
                "average": 0
              },
              "brand": {
                "name": "S-Style Möbel"
              },
              "breadcrumbs": [
                {
                  "name": "Startseite"
                },
                {
                  "name": "Möbel"
                },
                {
                  "name": "Sofas & Couches"
                },
                {
                  "name": "Wohnlandschaften"
                }
              ],
              "campaigns": [
                {
                  "key": "MP-special"
                }
              ],
              "variantName": "Hellgrau",
              "energyClass": null,
              "sponsored": null,
              "variations": [
                {
                  "renderingType": "icon",
                  "key": "colorSubcolor",
                  "title": "Farbe",
                  "groups": [
                    {
                      "values": [
                        {
                          "sku": "000000003000545472",
                          "imageUrl": "/subcolor-4175-421323.png",
                          "selected": false
                        },
                        {
                          "sku": "000000003000545474",
                          "imageUrl": "/subcolor-4289-421426.png",
                          "selected": false
                        },
                        {
                          "sku": "000000003000545477",
                          "imageUrl": "/subcolor-4314-421473.png",
                          "selected": false
                        },
                        {
                          "sku": "000000003000545473",
                          "imageUrl": "/subcolor-8215-421524.png",
                          "selected": true
                        },
                        {
                          "sku": "000000003000545475",
                          "imageUrl": "/subcolor-4198-421611.png",
                          "selected": false
                        },
                        {
                          "sku": "000000003000545476",
                          "imageUrl": "/subcolor-4504-421705.png",
                          "selected": false
                        }
                      ]
                    }
                  ]
                }
              ],
              "taxonomyBreadcrumbs": [
                {
                  "name": "Startseite",
                  "url": "/"
                },
                {
                  "name": "Möbel",
                  "url": "moebel-sortiment/"
                },
                {
                  "name": "Sofas & Couches",
                  "url": "sofa-couch/"
                },
                {
                  "name": "Wohnlandschaften",
                  "url": "wohnlandschaften/"
                }
              ]
            },
            {
              "sku": "000000001000076456",
              "parentSku": "000000008000008302",
              "url": "produkt/schwebetuerenschrank-quadra-mit-spiegel-alpinweiss-breite-x-hoehe-136-x-210-cm-136-x-210-cm",
              "name": "Schwebetürenschrank Quadra (mit Spiegel)",
              "trackingId": null,
              "images": [
                {
                  "path": "https://cdn1.home24.net/images/media/catalog/product/original/png/s/c/schwebetuerenschrank-quadra-mit-spiegel-alpinweiss-breite-x-hoehe-136-x-210-cm-4502144.webp"
                }
              ],
              "labels": [],
              "delivery": {
                "flags": {
                  "fastDelivery": false
                },
                "time": {
                  "numberOfDays": 24
                }
              },
              "prices": {
                "regular": {
                  "value": 39999,
                  "isRrp": false
                },
                "special": null
              },
              "sellable": true,
              "ratings": {
                "count": 174,
                "average": 4.3
              },
              "brand": {
                "name": "rauch Orange"
              },
              "breadcrumbs": [
                {
                  "name": "Startseite"
                },
                {
                  "name": "Möbel"
                },
                {
                  "name": "Schränke"
                },
                {
                  "name": "Kleiderschränke"
                }
              ],
              "campaigns": [
                {
                  "key": "influencer"
                },
                {
                  "key": "nonspecial-mp-reduced"
                },
                {
                  "key": "nonspecial-nonmp"
                },
                {
                  "key": "xdgspx-googleshopping"
                },
                {
                  "key": "meinestadt"
                },
                {
                  "key": "sustainable"
                },
                {
                  "key": "nonmp"
                }
              ],
              "variantName": "Alpinweiß - Breite x Höhe: 136 x 210 cm - 136 x 210 cm",
              "energyClass": null,
              "sponsored": null,
              "variations": [
                {
                  "renderingType": "text",
                  "key": "_width_height",
                  "title": "Breite x Höhe",
                  "groups": [
                    {
                      "values": [
                        {
                          "sku": "000000001000076456",
                          "imageUrl": null,
                          "selected": true
                        },
                        {
                          "sku": "000000001000079874",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000001000076457",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000001000079875",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000001000076458",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000001000079876",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000001000076459",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000001000079877",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000001000076460",
                          "imageUrl": null,
                          "selected": false
                        },
                        {
                          "sku": "000000001000079878",
                          "imageUrl": null,
                          "selected": false
                        }
                      ]
                    }
                  ]
                }
              ],
              "taxonomyBreadcrumbs": [
                {
                  "name": "Startseite",
                  "url": "/"
                },
                {
                  "name": "Möbel",
                  "url": "moebel-sortiment/"
                },
                {
                  "name": "Schränke",
                  "url": "schraenke-stauraum/"
                },
                {
                  "name": "Kleiderschränke",
                  "url": "alle-kleiderschraenke/"
                }
              ]
            },
            {
              "sku": "000000003000377366",
              "parentSku": "000000008500069355",
              "url": "produkt/fernsehtisch-ravennaf-200x42x58-beige-schwarz",
              "name": "Fernsehtisch RavennaF 200x42x58",
              "trackingId": null,
              "images": [
                {
                  "path": "https://cdn1.home24.net/images/media/catalog/product/original/jpg/4/8/4810c707eaba4f0e93f3f8e6aa651372.webp"
                }
              ],
              "labels": [],
              "delivery": {
                "flags": {
                  "fastDelivery": false
                },
                "time": {
                  "numberOfDays": 41
                }
              },
              "prices": {
                "regular": {
                  "value": 42000,
                  "isRrp": false
                },
                "special": null
              },
              "sellable": true,
              "ratings": {
                "count": 0,
                "average": 0
              },
              "brand": {
                "name": "AKL furniture"
              },
              "breadcrumbs": [
                {
                  "name": "Startseite"
                },
                {
                  "name": "Möbel"
                },
                {
                  "name": "TV-Möbel"
                },
                {
                  "name": "TV-Lowboards"
                }
              ],
              "campaigns": [
                {
                  "key": "MP-special"
                }
              ],
              "variantName": "Beige - Schwarz",
              "energyClass": null,
              "sponsored": null,
              "variations": [
                {
                  "renderingType": "icon",
                  "key": "colorSubcolor",
                  "title": "Farbe",
                  "groups": [
                    {
                      "values": [
                        {
                          "sku": "000000003000377396",
                          "imageUrl": "/subcolor-4175-421323.png",
                          "selected": false
                        },
                        {
                          "sku": "000000003000377366",
                          "imageUrl": "/subcolor-4175-421323.png",
                          "selected": true
                        },
                        {
                          "sku": "000000003000377378",
                          "imageUrl": "/subcolor-4194-421333.png",
                          "selected": false
                        },
                        {
                          "sku": "000000003000377357",
                          "imageUrl": "/subcolor-4194-421333.png",
                          "selected": false
                        },
                        {
                          "sku": "000000003000226169",
                          "imageUrl": "/subcolor-4194-421333.png",
                          "selected": false
                        },
                        {
                          "sku": "000000003000377387",
                          "imageUrl": "/subcolor-4194-421333.png",
                          "selected": false
                        },
                        {
                          "sku": "000000003000226078",
                          "imageUrl": "/subcolor-4194-421333.png",
                          "selected": false
                        },
                        {
                          "sku": "000000003000377415",
                          "imageUrl": "/subcolor-4194-421333.png",
                          "selected": false
                        }
                      ]
                    }
                  ]
                }
              ],
              "taxonomyBreadcrumbs": [
                {
                  "name": "Startseite",
                  "url": "/"
                },
                {
                  "name": "Möbel",
                  "url": "moebel-sortiment/"
                },
                {
                  "name": "TV-Möbel",
                  "url": "fernseh-tv-moebel/"
                },
                {
                  "name": "TV-Lowboards",
                  "url": "tv-lowboards/"
                }
              ]
            },
            {
              "sku": "000000001000209294",
              "parentSku": "000000001000209294-P",
              "url": "produkt/schwebetuerenschrank-niceville-iii-mit-spiegel",
              "name": "Schwebetürenschrank Niceville III",
              "trackingId": null,
              "images": [
                {
                  "path": "https://cdn1.home24.net/images/media/catalog/product/original/png/-/1/-1000209294-200406-14520900002-IMAGE-P000000001000209294.webp"
                }
              ],
              "labels": [
                {
                  "type": "campaign",
                  "value": "EXTRA -12 %"
                }
              ],
              "delivery": {
                "flags": {
                  "fastDelivery": true
                },
                "time": {
                  "numberOfDays": 10
                }
              },
              "prices": {
                "regular": {
                  "value": 29999,
                  "isRrp": false
                },
                "special": null
              },
              "sellable": true,
              "ratings": {
                "count": 12,
                "average": 4.4
              },
              "brand": {
                "name": "loftscape"
              },
              "breadcrumbs": [
                {
                  "name": "Startseite"
                },
                {
                  "name": "Möbel"
                },
                {
                  "name": "Schränke"
                },
                {
                  "name": "Kleiderschränke"
                }
              ],
              "campaigns": [
                {
                  "key": "xhcgspx-flash12de-0823"
                },
                {
                  "key": "specialdeals"
                },
                {
                  "key": "h24campaigns"
                },
                {
                  "key": "nonmp"
                }
              ],
              "variantName": "mit Spiegel",
              "energyClass": null,
              "sponsored": null,
              "variations": null,
              "taxonomyBreadcrumbs": [
                {
                  "name": "Startseite",
                  "url": "/"
                },
                {
                  "name": "Möbel",
                  "url": "moebel-sortiment/"
                },
                {
                  "name": "Schränke",
                  "url": "schraenke-stauraum/"
                },
                {
                  "name": "Kleiderschränke",
                  "url": "alle-kleiderschraenke/"
                }
              ]
            }
          ],
          "count": 54,
          "totalCount": 44758
        }
      }
    ]
  }