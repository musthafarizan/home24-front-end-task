import express, { Application } from 'express';
import categories from './data/categories'

const app: Application = express();

app.use((req, res, next) => {
  setTimeout(() => {
    next()
  }, 3000)
})

app.post('/graphql', (req, res) => {
  res.json({data: categories})
});
app.listen(3001);
